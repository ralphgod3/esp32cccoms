--esp32 address with uri to write to display CHANGE THE IP
--if it is still not connected make sure that you disabled the rule that blocks private connections in the computercraft config
local address = "http://192.168.0.211/display"






local function printTextBuffer(textBuffer)
    term.clear()
    term.setCursorPos(1,1)
    for i = 1, #textBuffer do
        term.setCursorPos(1,i)
        term.write(textBuffer[i])
    end
end

local function transmitTextBuffer(textBuffer, address)
    local toSend = ""
    for i = 1, #textBuffer do
        toSend = toSend .. textBuffer[i] .. "\n"
    end
    local ret = {http.post(address, toSend)}
end




local textBuffer = {}
textBuffer[1] = ""
local line = 1
while true do
    local eventInfo = {}
    repeat
        eventInfo = {os.pullEvent()}
    until eventInfo[1] == "key" or eventInfo[1] == "char"

    if eventInfo[1] == "char" then
        textBuffer[line] = textBuffer[line] .. eventInfo[2]
        printTextBuffer(textBuffer)
        transmitTextBuffer(textBuffer,address)
    elseif eventInfo[1] == "key" then
        if eventInfo[2] == keys.enter then
            line = line + 1
            textBuffer[line] = ""
            if line >= 5 then
                table.remove(textBuffer, 1)
                line = 4
            end
            printTextBuffer(textBuffer)
            transmitTextBuffer(textBuffer,address)
        end
    end
end