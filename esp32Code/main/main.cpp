//all the code is shite and you should not use it.
//this is just a way to display how one would interact with real world gear (in this case a microcontroller) from computercraft
//and yes the instances below are created with a "global" scope in this file. however unlike in arduinos shitty compile method that does not mean they are trully global.
//instead as in any sane language they can only be used in this file.

//display defines
#define I2C_SDA_PIN Gpio_t::NUM_5
#define I2C_SCL_PIN Gpio_t::NUM_4
#define DISPLAY_ADDR 0x3c

#define WIFI_SSID "example_ssid"
#define WIFI_PASSWORD "example_password"
#define HTTP_PORT 80

#include "nvs.h"
#include "nvs_flash.h"
#include "driver/ledc.h"
#include "../components/Gpio/src/ESP32/Gpio.h"
#include "../components/pininterrupts/src/ESP32/PinInterrupt.h"
#include "../components/Delay/src/ESP32/delay.h"
#include "../components/simpledebug/src/SimpleDebug.h"
#include "../components/mstream/src/ESP32/uart/esp32Uart.h"
#include "../components/internet/src/esp32/WifiDriverSta.h"
#include "../components/i2c/src/ESP32/esp32_i2c.h"
#include "../components/ssd1306display/src/SSD1306Wire.h"
#include "../components/dynamicarray/src/dynamicArray.h"
#include "esp_http_server.h"
#include "algorithm"

//initialize a bunch of stuff
DbgRegisterTag("MAIN");
//basic hardware
Delay dlay = Delay();
//anything with GetInstance means its a singleton ie only 1 instance can ever exist in the program.
//this is usually done for hardware as is the case here to prevent multiple instances of fighting with eachother
//in cpp & indicates a reference, a reference can NOT be changed after initialization
// * indicates a pointer which is a reference to a memory location but the memory location can be invalid ie more unsafe.
Gpio &gpio = Gpio::GetInstance();
PinInterrupt &intr = PinInterrupt::GetInstance();
esp32Uart uart = esp32Uart();
WifiDriverSta& wifi = WifiDriverSta::GetInstance();
ESP32I2c espI2c = ESP32I2c();
SSD1306Wire display = SSD1306Wire();



//copied from esp-idf example and slightly modified to write data to display
static esp_err_t displayPostHandler(httpd_req_t *req)
{
    char buf[1024];
    int ret, remaining = req->content_len;

    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, buf, std::min<int>(remaining, sizeof(buf)))) <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }
        remaining -= ret;
    }
    DbgLogI("=========== RECEIVED DATA ==========\r\n");
    DbgLogfI("%.*s\r\n", req->content_len, buf);
    DbgLogI("====================================\r\n");
    //If you want to bitch about me using DynamicArray instead of the standard std:vector go ahead, there is a reason however.
    //basicly the libraries im using also work on very shitty systems like an arduino nano which do not have the C++ standard library.
    //so i made my own implementation for some of the stuff i needed 
    DynamicArray<std::string> lines = DynamicArray<std::string>();
    int curLine = 0;
    //split lines on \n
    lines.push_back("");
    for(int i = 0; i < req->content_len; i ++)
    {
        if(buf[i] != '\n')
        {
            lines[curLine] = lines[curLine] + (buf[i]);
        }
        else
        {
            curLine++;
            lines.push_back("");
        }
    }

    display.clear();
    //we can only fit 4 lines but im too lazy to check for it here so ill just put that on the program sending the data, told you this code was shit
    for(int i = 0; i < lines.size(); i++)
    {
        display.drawString(0, i*16, lines[i]);
    }
    display.display();

    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

//information for the http server on when to call the callback function
static const httpd_uri_t toDisplay = {
    .uri       = "/display",
    .method    = HTTP_POST,
    .handler   = displayPostHandler,
    .user_ctx  = NULL
};







//main function, is extern C because by default esp-idf searches for app_main linked as a c function and this is a cpp file
extern "C" void app_main(void)
{
    //initialize hardware
    //uart is used for debug output
    uart.Init(UART_NUM_0, (gpio_num_t)UART_PIN_NO_CHANGE, (gpio_num_t)UART_PIN_NO_CHANGE);
    DbgInit(&uart);
    //i2c is used for display
    espI2c.Init(I2C_NUM_0, I2C_SDA_PIN, I2C_SCL_PIN);
    display.Init(DISPLAY_ADDR, &espI2c, &dlay, nullptr);
    display.setFont(ArialMT_Plain_16);
    display.drawString(0,0,"initializing");
    display.display();

    //initialize default event loop and non volatile storage which are both needed for wifi.
    esp_event_loop_create_default();
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
    //start wifi
    wifi.Init(WIFI_SSID, WIFI_PASSWORD);
    DbgLogV("waiting for wifi to connect\r\n");
    wifi.WaitForConnection();
    DbgLogV("connection completed\r\n")
    //start http server
    httpd_handle_t serverHandle = NULL;
    httpd_config config = HTTPD_DEFAULT_CONFIG();
    config.server_port = HTTP_PORT;
    DbgLogfI("starting server on port %d\r\n", config.server_port);
    httpd_start(&serverHandle, &config);
    httpd_register_uri_handler(serverHandle, &toDisplay);
    //display wifi info on display for user
    esp_ip4_addr addr = wifi.GetIpInfo().ip;
    DbgLogfI("IP: %d.%d.%d.%d \r\n", IP2STR(&addr));
    char tempStrBuf[100];
    display.drawStringf(0,16, tempStrBuf, "IP: %d.%d.%d.%d", IP2STR(&addr));
    display.display();

    //in FreeRTOS which esp-idf is based on it does not matter if the main function ends since the task scheduler will keep running above this.
    //basicly we dont have anything left to do synchronously after this so we end the main loop and wait for requests :)
}
