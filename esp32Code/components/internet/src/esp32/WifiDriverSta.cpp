//global includes
#include <string.h>
//esp-idf includes
#include "esp_log.h"
//#include "esp_netif.h"
//project includes
#include "WifiDriverSta.h"

/****************************************************************************
Private variables
****************************************************************************/

wifiStatus_t WifiDriverSta::s_wifiStatus = wifiStatus_t();
esp_netif_ip_info_t WifiDriverSta::s_ipInfo = esp_netif_ip_info_t();
EventGroupHandle_t WifiDriverSta::s_wifi_event_group = EventGroupHandle_t();
int WifiDriverSta::s_currentRetry = 0;
std::string WifiDriverSta::s_hostname = "";

/****************************************************************************
Defines
****************************************************************************/

/****************************************************************************
Public Functions
****************************************************************************/

WifiDriverSta& WifiDriverSta::GetInstance()
{
    static WifiDriverSta s_Wifi;
    return s_Wifi;
}

void WifiDriverSta::Connect()
{
    esp_wifi_connect();
}

void WifiDriverSta::Deinit()
{
    Disconnect();
    esp_wifi_stop();
    esp_wifi_deinit();
}

void WifiDriverSta::Disconnect()
{
    esp_wifi_disconnect();
}

uint8_t WifiDriverSta::GetChannel()
{
    uint8_t lv_chan;
    wifi_second_chan_t lv_chan2;
    esp_wifi_get_channel(&lv_chan, &lv_chan2);
    return lv_chan;
}

wifi_sta_config_t WifiDriverSta::GetConfig()
{
    wifi_config_t lv_conf;
    esp_wifi_get_config(wifi_interface_t::WIFI_IF_STA, &lv_conf);
    return lv_conf.sta;
}

esp_netif_ip_info_t WifiDriverSta::GetIpInfo()
{
    return s_ipInfo;
}

std::string WifiDriverSta::GetHostname()
{
    return s_hostname;
}

std::array<uint8_t, 6> WifiDriverSta::GetMac()
{
    std::array<uint8_t, 6> lv_mac;
    esp_wifi_get_mac(wifi_interface_t::WIFI_IF_STA, lv_mac.data());
    return lv_mac;
}

wifiStatus_t WifiDriverSta::GetStatus()
{
    return s_wifiStatus;
}

StatusCode_t WifiDriverSta::Init(std::string ssid, std::string password)
{
    if (m_running)
    {
        return StatusCode_t::ALREADY_INITIALIZED;
    }

    if (ssid.length() > 32 || password.length() > 64)
    {
        return StatusCode_t::INVALID_ARG;
    }
    esp_err_t lv_err = esp_netif_init();
    esp_netif_create_default_wifi_sta();

    if (lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }
    //create wifiEvent group
    s_wifi_event_group = xEventGroupCreate();
    wifi_init_config_t lv_cfg = WIFI_INIT_CONFIG_DEFAULT();
    lv_err = esp_wifi_init(&lv_cfg);
    if (lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }
    //registering event handlers for wifi
    lv_err = esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &EventHandlerWifi, NULL, NULL);
    if (lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }
    lv_err = esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &EventHandlerWifi, NULL, NULL);
    if (lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }
    lv_err = esp_wifi_set_mode(WIFI_MODE_STA);
    if (lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }


    //set configuration
    wifi_config_t lv_config = wifi_config_t();
    memcpy(lv_config.sta.ssid, ssid.data(), ssid.length());
    memcpy(lv_config.sta.password, password.data(), password.length());
    lv_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
    lv_config.sta.pmf_cfg.capable = true;
    lv_config.sta.pmf_cfg.required = false;
    
    lv_err = esp_wifi_set_config(wifi_interface_t::WIFI_IF_STA, &lv_config);
    if (lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }
    //start wifi
    lv_err = esp_wifi_start();
    if (lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }
    m_running = true;
    return StatusCode_t::OK;
}

StatusCode_t WifiDriverSta::WaitForConnection()
{
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group, s_WIFI_CONNECTED_BIT | s_WIFI_FAIL_BIT, pdFALSE, pdFALSE, portMAX_DELAY);
    if (bits & s_WIFI_CONNECTED_BIT)
    {
        return StatusCode_t::OK;
    }
    return StatusCode_t::FAIL;
}

std::vector<wifi_ap_record_t> WifiDriverSta::Scan(wifi_scan_config_t scanConfig)
{
    esp_err_t lv_err = esp_wifi_scan_start(&scanConfig, true);
    if (lv_err == ESP_ERR_WIFI_TIMEOUT)
    {
        return std::vector<wifi_ap_record_t>();
    }
    else if (lv_err == ESP_ERR_WIFI_STATE)
    {
        return std::vector<wifi_ap_record_t>();
    }
    uint16_t lv_apRecordNum;
    DbgLogfI("records: %i\r\n", lv_apRecordNum);
    lv_err = esp_wifi_scan_get_ap_num(&lv_apRecordNum);
    if(lv_err != ESP_OK)
    {
        return std::vector<wifi_ap_record_t>();
    }
    
    wifi_ap_record_t* records = new wifi_ap_record_t[lv_apRecordNum];
    lv_err = esp_wifi_scan_get_ap_records(&lv_apRecordNum, records);
    esp_wifi_scan_stop();
    if (lv_err != ESP_OK)
    {
        delete[] records;
        return std::vector<wifi_ap_record_t>();
    }
    //prepare return value
    std::vector<wifi_ap_record_t> lv_ret;
    for (int lv_i = 0; lv_i < lv_apRecordNum; lv_i++)
    {
        lv_ret.push_back(records[lv_i]);
    }
    delete[] records;
    records = nullptr;
    return lv_ret;
}

StatusCode_t WifiDriverSta::SetConfig(std::string ssid, std::string password)
{
    if (ssid.length() > 32 || password.length() > 64)
    {
        return StatusCode_t::INVALID_ARG;
    }

    //set configuration
    wifi_config_t lv_config = wifi_config_t();
    memcpy(lv_config.sta.ssid, ssid.data(), ssid.length());
    memcpy(lv_config.sta.password, password.data(), password.length());
    esp_err_t lv_err = esp_wifi_set_config(wifi_interface_t::WIFI_IF_STA, &lv_config);
    esp_wifi_stop();
    esp_wifi_start();
    if(lv_err != ESP_OK)
    {
        return ConvertEspErrorToStatusCode(lv_err);
    }

    SetHostname(s_hostname);
    return StatusCode_t::OK;
}

StatusCode_t WifiDriverSta::SetHostname(std::string hostname)
{
    if (hostname.size() > 32 || hostname.size() < 1)
    {
        return StatusCode_t::INVALID_ARG;
    }
    //esp_err_t lv_err =  esp_netif_set_hostname(sta_netif,hostname.c_str());
    esp_err_t lv_err = tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, hostname.c_str());
    s_hostname = hostname;
    return ConvertEspErrorToStatusCode(lv_err);
}

StatusCode_t WifiDriverSta::SetMac(std::array<uint8_t, 6> mac)
{
    //mac cannot have bit 0 of byte 0 be 1.
    if (mac[0] &= 1)
    {
        return StatusCode_t::INVALID_ARG;
    }
    esp_err_t lv_err = 0;
    esp_wifi_stop();
    lv_err = esp_wifi_set_mac(wifi_interface_t::WIFI_IF_STA, mac.data());
    esp_wifi_start();

    return ConvertEspErrorToStatusCode(lv_err);
}

/****************************************************************************
Protected Functions
****************************************************************************/

WifiDriverSta::WifiDriverSta(): m_running(false)
{
    s_hostname = "Espressif";
}

WifiDriverSta::~WifiDriverSta()
{
    if (m_running)
    {
        Deinit();
    }
}

/****************************************************************************
Private Functions
****************************************************************************/
void WifiDriverSta::EventHandlerWifi(void *arg, esp_event_base_t eventBase, int32_t eventID, void *eventData)
{
    s_wifiStatus.eventBase = eventBase;
    s_wifiStatus.eventID = eventID;
    s_wifiStatus.eventData = eventData;
    DbgLogfV("event: %s id: %d\r\n", eventBase, eventID);
    if (eventBase == WIFI_EVENT)
    {
        switch (eventID)
        {
        case WIFI_EVENT_STA_START:
            esp_wifi_connect();
            break;
        case WIFI_EVENT_STA_DISCONNECTED:
        {
            //work around for known issue in esp-idf where wifi does not reconnect after a disconnect
            //
            if(s_currentRetry > 10)
            {
                s_currentRetry = 0;

                esp_wifi_disconnect();
                esp_wifi_stop();
                WifiDriverSta::GetInstance().SetHostname(s_hostname);
                esp_wifi_start();
                esp_wifi_connect(); 
                
            }
            esp_wifi_connect();
            xEventGroupClearBits(s_wifi_event_group, s_WIFI_CONNECTED_BIT);
            s_currentRetry++;
            break;
        }
        default:
            break;
        }
    }
    else if (eventBase == IP_EVENT && eventID == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) eventData;
        DbgLogfV("Got ip: %d.%d.%d.%d\r\n ", IP2STR(&event->ip_info.ip))
        s_ipInfo.gw = event->ip_info.gw;
        s_ipInfo.ip = event->ip_info.ip;
        s_ipInfo.netmask = event->ip_info.netmask;
        s_currentRetry = 0;
        xEventGroupSetBits(s_wifi_event_group, s_WIFI_CONNECTED_BIT);
    }
}