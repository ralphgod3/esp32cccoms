#ifndef WIFIDRIVER_STA_H
#define WIFIDRIVER_STA_H


/****************************************************************************
Includes
****************************************************************************/
//global includes
#include <vector>
#include <array>
#include <string>
//esp-idf includes
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_event.h"
#include "esp_wifi.h"
#include "esp_netif.h"

#include "../../../statuscodesandexceptions/src/StatusCodes.h"
#include "../../../simpledebug/src/SimpleDebug.h"

/**
 * @brief wifistatus struct
 * 
 */
struct wifiStatus_t
{
    int32_t eventID;            /*!< eventID for the given event */
    esp_event_base_t eventBase; /*!< base event, should be WIFI or TCPIP, can be accessed as a const char* when used in printf functions */
    void *eventData;            /*!< data associated with the event */
};

/**
 * @brief handles wifi and the tcp ip stack running on the esp32
 * 
 */
class WifiDriverSta final
{
private:
    bool m_running;
    DbgRegisterTag("WiFi STA");

    static wifiStatus_t s_wifiStatus;
    static esp_netif_ip_info_t s_ipInfo;
    static EventGroupHandle_t s_wifi_event_group;
    static const int s_WIFI_CONNECTED_BIT = BIT0;
    static const int s_WIFI_FAIL_BIT = BIT1;
    static int s_currentRetry;
    static std::string s_hostname;
    //esp_netif_t *sta_netif;

    //block copying of object
    WifiDriverSta(const WifiDriverSta&) = delete; // copy constructor
    WifiDriverSta(WifiDriverSta&&) = delete; // copy constructor
    WifiDriverSta& operator=(const WifiDriverSta&) = delete; //copy assign
    WifiDriverSta& operator=( WifiDriverSta&&) = delete; //move assign

    static void EventHandlerWifi(void *arg, esp_event_base_t eventBase, int32_t eventID, void *eventData);

protected:

    /**
     * @brief Construct a new Wifi object
     * take note that WIFI needs NVS to operate so initialize it before starting wifi
     * snippet:
     * //initalize event loop
     * esp_event_loop_create_default();
     * //Initialize NVS
     *   esp_err_t ret = nvs_flash_init();
     *   if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
     *   ESP_ERROR_CHECK(nvs_flash_erase());
     *   ret = nvs_flash_init();
     *   }
     *   ESP_ERROR_CHECK(ret);
     */
    WifiDriverSta();
    
    /**
     * @brief Destroy the Wifi object
     * 
     */
    ~WifiDriverSta();

public:
    static WifiDriverSta& GetInstance();

    /**
     * @brief connect to wifi network in the configuration
     * 
     */
    void Connect();

    /**
     * @brief completely dealocate all recources that where used for wifi
     * 
     */
    void Deinit();

    /**
     * @brief disconnect from wifi network
     * 
     */
    void Disconnect();

    /**
     * @brief Get the primary wifi channel
     * 
     * @return uint8_t channel
     */
    uint8_t GetChannel();

    /**
     * @brief Get the Configuration of wifi
     * 
     * @return wifi_sta_config_t wifi configuration
     */
    wifi_sta_config_t GetConfig();

    /**
     * @brief Get the Hostname
     * 
     * @return std::string hostName
     */
    std::string GetHostname();

    /**
     * @brief Get the Ip Information
     * 
     * @return esp_netif_ip_info_t  ip information
     */
    esp_netif_ip_info_t GetIpInfo();

    /**
     * @brief Get the Mac address
     * 
     * @return std::array<uint8_t,6> macadress 
     */
    std::array<uint8_t, 6> GetMac();

    /**
     * @brief Get the current wifi status
     * 
     * @return wifiStatus_t latest status change with all event info associated with it.
     */
    wifiStatus_t GetStatus();

    /**
     * @brief initialize wifi, has to be called first before all other functions
     * 
     * @param ssid name of network to connect with
     * @param password password of network to connect with
     * @return StatusCode_t status code
     */
    StatusCode_t Init(std::string ssid, std::string password);


    /**
     * @brief wait until wifi connects or fails to connect
     * 
     * @return StatusCode_t OK on connection fail on failure
     */
    StatusCode_t WaitForConnection();

    /**
     * @brief scan for networks
     * before calling this ensure that esp_event_loop_create_default() is called, wifi will fail silently otherwise
     * @param scanConfig config that the APs have to meet to be recorded in the scan
     * @return std::vector<wifi_ap_record_t> vector holding all APs that are found
     */
    std::vector<wifi_ap_record_t> Scan(wifi_scan_config_t scanConfig = wifi_scan_config_t());

    /**
     * @brief Set the Configuration of wifi
     * 
     * @param ssid name of network to connect with
     * @param password password of network to connect with
     * @return StatusCode_t status code
     */
    StatusCode_t SetConfig(std::string ssid, std::string password);

    /**
     * @brief Set the hostname for the device
     * 
     * @param hostname string max length 32 bytes
     * @return StatusCode_t status code
     */
    StatusCode_t SetHostname(std::string hostname);

    /**
     * 
     * @brief Set the Mac address
     * esp32 will go offline for a while whilst setting the MAC address
     * bit 0 of byte 0 can not be 1
     * @param mac macaddress
     * @return StatusCode_t statuscode
     */
    StatusCode_t SetMac(std::array<uint8_t, 6> mac);

};
#endif