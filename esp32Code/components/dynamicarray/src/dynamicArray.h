#ifndef __DYNAMIC_ARRAY_H__
#define __DYNAMIC_ARRAY_H__
#include <stdint.h>
#include "../../statuscodesandexceptions/src/StatusCodes.h"


/**
 * @brief generic template implementation for a dynamic array
 * 
 * @tparam T whatever you wish to store
 */
template <class T>
class DynamicArray
{
private:
    //holds head of array
    T* m_Head = nullptr;
    //amount of items that can be stored in array
    uint32_t m_Count = 0;
    /**
     * @brief internal find function, matches objects base on == operator
     * 
     * @param toMatch object to find
     * @param startIndex index to start searching from
     * @return int32_t index object was found at or -1
     */
    int32_t findInternal(T toMatch, uint32_t startIndex = 0)
    {
        for(uint32_t i = startIndex; i < size(); i++)
        {
            if(m_Head[i] == toMatch)
            {
                return i;
            }
        }
        return -1;
    }

    /**
     * @brief internal find function that can search with an externally declared function
     * 
     * @param MatchingFunction function that will be used to select a match
     * @param toMatch object to search for
     * @param startIndex index to start at
     * @return int32_t index object was found at or -1
     */
    int32_t findInternal(bool (*MatchingFunction)(T obj1, T obj2), T toMatch, uint32_t startIndex = 0)
    {
        for(uint32_t i = startIndex; i < size(); i++)
        {
            if(MatchingFunction(m_Head[i], toMatch))
            {
                return i;
            }
        }
        return -1;
    }

    /**
     * @brief deep copies other object to this
     * 
     * @param other object to copy 
     * @return DynamicArray<T>& this
     */
    DynamicArray<T>& internalCopy(DynamicArray& other)
    {
        if(this == &other)
        {
            return *this;
        }
        if(m_Head != nullptr)
        {
            delete[] m_Head;
            m_Head = nullptr;
        }
        if(other.m_Count != 0 && other.m_Head != nullptr)
        {
            this->m_Count = other.m_Count;
            this->m_Head = new T[this->m_Count];

            for(uint32_t i = 0; i< this->m_Count; i++)
            {
                this->m_Head[i] = other.m_Head[i];
            }
        }
        return *this;
    }

public:
    /**
     * @brief Construct a new DynamicArray object
     * 
     */
    DynamicArray()
    {
        m_Count = 0;
        m_Head = nullptr;
    }

    /**
     * @brief Construct a new Dynamic Array object
     * 
     * @param other object to copy
     */
    DynamicArray(const DynamicArray &other) // copy constructor
    {
        internalCopy(const_cast<DynamicArray<T>&>(other));
    }

    /**
     * @brief Construct a new Dynamic Array object
     * 
     * @param other object to copy
     */
    DynamicArray(DynamicArray &&other) // copy constructor
    {
        internalCopy(other);
    }

    /**
     * @brief copy assign
     * 
     * @param other object to copy from
     * @return DynamicArray& this
     */
    DynamicArray &operator=(const DynamicArray & other) //copy assign
    {
        return internalCopy(const_cast<DynamicArray<T>&>(other));
    }

    /**
     * @brief move assign
     * 
     * @param other object to copy from
     * @return DynamicArray& this
     */
    DynamicArray &operator=(DynamicArray && other) //move assign
    {
        return internalCopy(other);
    }

    /**
     * @brief Destroy the DynamicArray object
     * 
     */
    ~DynamicArray()
    {
        if(m_Head != nullptr)
        {
            delete[] m_Head;
            m_Head = nullptr;
        }
    }

    /**
     * @brief Get the Head of the dynamic array, useful for easy copy operations
     * 
     * @return T& head of dynamic array
     */
    T *getHead()
    {
        return m_Head;
    }

    /**
     * @brief insert data at specified point into array
     * 
     * @param index point at which to insert the data
     * @param data data to insert
     * @param size size of the data to insert
     * @return StatusCode_t OK or Error
     */
    StatusCode_t insert(uint32_t index, T* data, uint32_t size)
    {
        if(index > m_Count)
        {
            return StatusCode_t::OUT_OF_RANGE;
        }
        if(size == 0 || data == nullptr)
        {
            return StatusCode_t::INVALID_ARG;
        }
        T* temp = new T[m_Count+size];
        
        //copy front
        if(m_Head != nullptr && index != 0)
        {
            for(uint32_t i = 0; i< index; i++)
            {
                temp[i] = m_Head[i];
            }
        }

        //copy new data
        for(uint32_t i = 0; i < size; i++)
        {
            temp[index + i] = data[i];
        }
        

        //copy back
        if(m_Head != nullptr && index != m_Count)
        {
            for(uint32_t i = 0; i< (m_Count + 1 - (index + size)); i++)
            {
                temp[index + size + i] = m_Head[index + i];
            }
        }
        
        if(m_Head != nullptr)
        {
            delete[] m_Head;
            m_Head = nullptr;
        }
        m_Count += size;
        m_Head = temp;
        return StatusCode_t::OK;
    }

    /**
     * @brief insert an element into the array
     * 
     * @param index index to insert element at
     * @param data element to insert
     * @return StatusCode_t OK or Error
     */
    StatusCode_t insert(uint32_t index, T data)
    {
        return insert(index, &data,1);
    }

    /**
     * @brief add elements to array at the back
     * 
     * @param data data for elements to add
     * @param size size of the data that will be added
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_back(T* data, uint32_t size)
    {
        return insert(m_Count, data, size);
    }

    /**
     * @brief add element to array at the back
     * 
     * @param data data for element
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_back(T data)
    {
        return push_back(&data,1);
    }

    /**
     * @brief adds elements to array at the front
     * 
     * @param data data for elements to add
     * @param size size of array to add data for
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_front(T* data, uint32_t size)
    {
        return insert(0, data, size);
    }

    /**
     * @brief add element to array at the front
     * 
     * @param data data for element
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_front(T data)
    {
        return push_front(&data,1);
    }

    /**
     * @brief delete N amount of elements from array
     * 
     * @param startIndex index that the deletion will start at
     * @param size amount of elements to delete
     * @return StatusCode_t OK or Error
     */
    StatusCode_t erase(uint32_t startIndex, uint32_t size)
    {
        if(startIndex > m_Count-1)
        {
            return StatusCode_t::OUT_OF_RANGE;
        }

        if(startIndex + size > m_Count-1)
        {
            size = m_Count-startIndex;
        }
        else if(size == 0)
        {
            return StatusCode_t::OK ;
        }

        T* temp = new T[m_Count-size];
        if(startIndex != 0)
        {
            for(uint32_t i = 0; i < startIndex; i++)
            {
                temp[i] = m_Head[i];
            }
        }
        if(startIndex != m_Count-1)
        {
            for(uint32_t i = 0; i< (m_Count - (startIndex + size)); i++)
            {
                temp[startIndex + i] = m_Head[startIndex + size + i];
            }
        }

        m_Count -= size;
        delete[] m_Head;
        m_Head = nullptr;
        m_Head = temp;
        return StatusCode_t::OK;

    }

    /**
     * @brief remove element from array
     * 
     * @param index index to remove
     * @return StatusCode_t OK or Error
     */
    StatusCode_t erase(uint32_t index)
    {
        return erase(index,1);
    }

    /**
     * @brief removes elements from front of the array
     * 
     * @param size amount of elements to remove
     * @return StatusCode_t statuscode
     */
    StatusCode_t pop_front(uint32_t size)
    {
        return erase(0, size);
    }

    /**
     * @brief removes element from front of the array
     * 
     * @return StatusCode_t statuscode
     */
    StatusCode_t pop_front()
    {
        return erase(0);
    }

    /**
     * @brief removes elements from back of array
     * 
     * @param size amount of elements to remove
     * @return StatusCode_t statuscode 
     */
    StatusCode_t pop_back(uint32_t size)
    {
        return erase(m_Count-1, size);
    }

    /**
     * @brief removes element from back of array
     * 
     * @return StatusCode_t statuscode 
     */
    StatusCode_t pop_back()
    {
        return erase(m_Count-1);
    }

    /**
     * @brief resizes the dynamic array to this size
     * @note if array was larger previously that data will be destroyed
     * @param size size to resize array too
     * @return StatusCode_t ok or error
     */
    StatusCode_t resize(uint32_t size)
    {
        T* tempHead = new T[size];
        for(uint32_t i = 0; i< size; i++)
        {
            tempHead[i] = T();
        }

        if(m_Head != nullptr)
        {
            if(m_Count <= size)
            {
                for(uint32_t i = 0; i< m_Count; i++)
                {
                    tempHead[i] = m_Head[i];
                }
            }
            else
            {
                for(uint32_t i = 0; i< size; i++)
                {
                    tempHead[i] = m_Head[i];
                }
            }
            delete[] m_Head;
            m_Head = nullptr;
        }
        m_Head = tempHead;
        m_Count = size;
        return StatusCode_t::OK;
    }

    /**
     * @brief return amount of elements in array
     * 
     * @return uint32_t amount of elements
     */
    uint32_t size()
    {
        return m_Count;
    }

    /**
     * @brief array operator overload
     * 
     * @param i index to acces
     * @return T& reference to element
     */
    T &operator[](uint32_t i)
    {
        return m_Head[i];
    }

    /**
     * @brief erase the entire array
     * 
     * @return ArrayError statuscode
     */
    StatusCode_t clear()
    {
        if(m_Head != nullptr)
        {
            delete[] m_Head;
            m_Head = nullptr;
            m_Count = 0;
        }
        return StatusCode_t::OK;
    }

    /**
     * @brief find with costumizable match function
     * 
     * @param MatchingFunction function that specifies when contained object matches
     * @param toMatch object to match with
     * @return int32_t -1 on no match index of first matching object otherwise
     */
    int32_t find(bool (*MatchingFunction)(T obj1, T obj2), T toMatch)
    {
        return FindInternal(MatchingFunction, toMatch, 0);
    }

    /**
     * @brief find with costumizable match function
     * 
     * @param MatchingFunction function that specifies when contained object matches
     * @param toMatch object to match with
     * @return DynamicArray<uint32_t> list of matching objects empty when no match is found
     */
    DynamicArray<uint32_t> findAll(bool (*MatchingFunction)(T obj1, T obj2), T toMatch)
    {
        DynamicArray<uint32_t> list = DynamicArray<uint32_t>();
        int32_t idx = 0;
        while (idx >= 0)
        {
            idx = findInternal(MatchingFunction, toMatch, idx);
            if (idx >= 0)
            {
                list.push_back(idx);
                idx += 1;
            }
        }
        return list;
    }

    /**
     * @brief simple find for matching values, returns first matching index for value in array
     * 
     * @param toMatch object or value to match with
     * @return int32_t -1 on not found index of element otherwise
     */
    int32_t find(T toMatch)
    {
        return findInternal(toMatch);
    }

    /**
     * @brief finds all elements that have the matching value
     * 
     * @param toMatch value to match with
     * @return DynamicArray<uint32_t> list of indexes with elements that match empty when no match is found
     */
    DynamicArray <uint32_t> findAll(T toMatch)
    {
        DynamicArray<uint32_t> list = DynamicArray<uint32_t>();
        int32_t idx = 0;
        while (idx >= 0)
        {
            idx = findInternal(toMatch, idx);
            if (idx >= 0)
            {
                list.push_back(idx);
                idx += 1;
            }
        }
        return list;
    }

    /**
     * @brief returns true if object is contained in array
     * 
     * @param toMatch object to match
     * @return true object is in array
     * @return false object is not in array
     */
    bool contains(T toMatch)
    {
        if(findInternal(toMatch) > -1)
        {
            return true;
        }
        return false;
    }
};



#endif