#include "DebounceInput.h"

StatusCode_t DebounceInput::Init(IDelay* delay, IGpio* gpio, Gpio_t pin, uint16_t debounceTime, bool inverseLogic, PullMode_t pullMode)
{
	if (p_initialized)
	{
		return StatusCode_t::NOT_INITIALIZED;
	}
	if (delay == nullptr|| gpio == nullptr)
	{
		return StatusCode_t::INVALID_ARG;
	}
	p_debounceTime = debounceTime;
	p_inverseLogic = inverseLogic;
	p_delay = delay;
	p_gpio = gpio;
	p_pin = pin;

	gpio->SetPinMode(pin, pullMode, GpioMode_t::Input);

	p_oldState = gpio->readPin(pin);

	p_initialized = true;
	return StatusCode_t::OK;
}

bool DebounceInput::IsPressed()
{
	if (!p_initialized)
	{
		return false;
	}
	Handle();

	if (p_inverseLogic)
	{
		return !p_oldState;
	}
	return p_oldState;
}



void DebounceInput::Handle()
{
	if (!p_initialized)
	{
		return;
	}

	bool state = p_gpio->readPin(p_pin);
	unsigned long curTime = p_delay->GetMillisSinceStartup();

	if (state != p_oldState && !p_timerStarted)
	{
		p_oldMillis = curTime;
		p_timerStarted = true;;
	}
	else if (curTime - p_oldMillis > p_debounceTime && p_timerStarted)
	{
		p_oldState = state;
		p_timerStarted = false;
	}
	
	
	
}