#ifndef __I_DEBOUNCEINPUT_H__
#define __I_DEBOUNCEINPUT_H__

#include "../../Gpio/src/IGpio.h"
#include "../../statuscodesandexceptions/src/StatusCodes.h"
#include "../../pininterrupts/src/IPinInterrupt.h"


class IDebounceInput
{
public:
    virtual ~IDebounceInput() = default;


    /**
	 * @brief returns if input is pressed,non blocking should be called alot in a loop
	 * 
	 * @return true input is pressed
	 * @return false input is not pressed
	 */

	virtual bool IsPressed() = 0;
};




#endif