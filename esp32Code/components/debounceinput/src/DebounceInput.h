#ifndef __DEBOUNCE_INPUT_H__
#define __DEBOUNCE_INPUT_H__

#include "IDebounceInput.h"
#include "../../Delay/src/IDelay.h"
/**
 * @brief supports easy debouncing of simple inputs like buttons
 * 
 */
class DebounceInput : public IDebounceInput
{
private:
	bool p_initialized = false;
	bool p_oldState = false;
	uint16_t p_oldMillis = 0;
	uint16_t p_debounceTime = 0;
	bool p_inverseLogic = false;
	IDelay* p_delay = nullptr;
	IGpio* p_gpio = nullptr;
	Gpio_t p_pin;
	bool p_timerStarted;
public:
	
	/**
	 * @brief intialize debounceInput
	 *
	 * @param delay delay instance
	 * @param gpio gpio instance
	 * @param pin pin that the input is corrected too
	 * @param time before an input is accepted as valid
	 * @param inverseLogic inverts the pressed state returned false means that high input returns true on isPressed
	 * @param pullMode INPUT or INPUT_PULLUP, best used with inverseLogic
	 * @return StatusCode_t status code
	 */
	StatusCode_t Init(IDelay* delay , IGpio* gpio, Gpio_t pin, uint16_t debounceTime, bool inverseLogic, PullMode_t pullMode);
	
	DebounceInput() {}
	~DebounceInput() {}
	/**
	 * @brief returns if input is pressed,non blocking should be called alot in a loop
	 * 
	 * @return true input is pressed
	 * @return false input is not pressed
	 */

	bool IsPressed();
	void Handle();
};

#endif // !DEBOUNCEINPOUT_H
