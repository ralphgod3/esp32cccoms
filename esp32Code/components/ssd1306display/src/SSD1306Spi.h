/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 by ThingPulse, Daniel Eichhorn
 * Copyright (c) 2018 by Fabrice Weinberg
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ThingPulse invests considerable time and money to develop these open source libraries.
 * Please support us by buying our products (and not the clones) from
 * https://thingpulse.com
 *
 */

#ifndef SSD1306Spi_h
#define SSD1306Spi_h

#include "OLEDDisplay.h"
#include "../../SPI/ISPI.h"
#include "../../Gpio/IGpio.h"
#include "../../Delay/IDelay.h"

class SSD1306Spi : public OLEDDisplay
{
private:
  IGpio *_rst = nullptr;
  IGpio *_dc = nullptr;
  IGpio *_cs = nullptr;
  ISPI *spi = nullptr;
  IDelay* delay = nullptr;

public:
  SSD1306Spi(ISPI *Spi, IGpio *_rst, IGpio *_dc, IGpio *_cs, IDelay* Delay, OLEDDISPLAY_GEOMETRY g = OLEDDISPLAY_GEOMETRY::GEOMETRY_128_64)
  {
    setGeometry(g);

    this->spi = Spi;
    this->_rst = _rst;
    this->_dc = _dc;
    this->_cs = _cs;
    this->delay = Delay;
  }

  bool connect()
  {
    // Pulse Reset low for 10ms
    _rst->WritePin(1);
    delay->ms(1);
    _rst->WritePin(0);
    delay->ms(10);
    _rst->WritePin(1);
    return true;
  }

  void display(void)
  {
#ifdef OLEDDISPLAY_DOUBLE_BUFFER
    uint8_t minBoundY = UINT8_MAX;
    uint8_t maxBoundY = 0;

    uint8_t minBoundX = UINT8_MAX;
    uint8_t maxBoundX = 0;

    uint8_t x, y;

    // Calculate the Y bounding box of changes
    // and copy buffer[pos] to buffer_back[pos];
    for (y = 0; y < (displayHeight / 8); y++)
    {
      for (x = 0; x < displayWidth; x++)
      {
        uint16_t pos = x + y * displayWidth;
        if (buffer[pos] != buffer_back[pos])
        {
          minBoundY = std::min(minBoundY, y);
          maxBoundY = std::max(maxBoundY, y);
          minBoundX = std::min(minBoundX, x);
          maxBoundX = std::max(maxBoundX, x);
        }
        buffer_back[pos] = buffer[pos];
      }
      delay->ms(1);
    }

    // If the minBoundY wasn't updated
    // we can savely assume that buffer_back[pos] == buffer[pos]
    // holdes true for all values of pos
    if (minBoundY == UINT8_MAX)
      return;

    sendCommand(COLUMNADDR);
    sendCommand(minBoundX);
    sendCommand(maxBoundX);

    sendCommand(PAGEADDR);
    sendCommand(minBoundY);
    sendCommand(maxBoundY);


    _cs->WritePin(1);
    _dc->WritePin(1);
    _cs->WritePin(0);
    for (y = minBoundY; y <= maxBoundY; y++)
    {
      for (x = minBoundX; x <= maxBoundX; x++)
      {
        spi->Transmit(&(buffer[x + y * displayWidth]),1,0);
      }
      delay->ms(1);
    }
    _cs->WritePin(1);
#else
    // No double buffering
    sendCommand(COLUMNADDR);
    sendCommand(0x0);
    sendCommand(0x7F);

    sendCommand(PAGEADDR);
    sendCommand(0x0);

    if (geometry == GEOMETRY_128_64 || geometry == GEOMETRY_64_48)
    {
      sendCommand(0x7);
    }
    else if (geometry == GEOMETRY_128_32)
    {
      sendCommand(0x3);
    }

    _cs->WritePin(1);
    _dc->WritePin(1);
    _cs->WritePin(0);
    for (uint16_t i = 0; i < displayBufferSize; i++)
    {
      spi->Transmit(&buffer[1],1,0);
      delay->ms(1);
    }
    _cs->WritePin(1);
#endif
  }

private:
  int getBufferOffset(void)
  {
    return 0;
  }
  inline void sendCommand(uint8_t com)
  {
    _cs->WritePin(1);
    _dc->WritePin(0);
    _cs->WritePin(0);
    spi->Transmit(&com,1,0);
    _cs->WritePin(1);
  }
};

#endif
