/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 by ThingPulse, Daniel Eichhorn
 * Copyright (c) 2018 by Fabrice Weinberg
 * Copyright (c) 2019 by Helmut Tschemernjak - www.radioshuttle.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ThingPulse invests considerable time and money to develop these open source libraries.
 * Please support us by buying our products (and not the clones) from
 * https://thingpulse.com
 *
 */

#ifndef OLEDDISPLAY_h
#define OLEDDISPLAY_h

//#define pgm_read_byte(addr)   (*(const unsigned char *)(addr))

#include "OLEDDisplayFonts.h"
#include <string>

//#define DEBUG_OLEDDISPLAY(...) Serial.printf( __VA_ARGS__ )
//#define DEBUG_OLEDDISPLAY(...) dprintf("%s",  __VA_ARGS__ )

#ifndef DEBUG_OLEDDISPLAY
#define DEBUG_OLEDDISPLAY(...)
#endif

// Use DOUBLE BUFFERING by default
#ifndef OLEDDISPLAY_REDUCE_MEMORY
#define OLEDDISPLAY_DOUBLE_BUFFER
#endif

// Header Values
#define JUMPTABLE_BYTES 4

#define JUMPTABLE_LSB 1
#define JUMPTABLE_SIZE 2
#define JUMPTABLE_WIDTH 3
#define JUMPTABLE_START 4

#define WIDTH_POS 0
#define HEIGHT_POS 1
#define FIRST_CHAR_POS 2
#define CHAR_NUM_POS 3

// Display commands
#define CHARGEPUMP 0x8D
#define COLUMNADDR 0x21
#define COMSCANDEC 0xC8
#define COMSCANINC 0xC0
#define DISPLAYALLON 0xA5
#define DISPLAYALLON_RESUME 0xA4
#define DISPLAYOFF 0xAE
#define DISPLAYON 0xAF
#define EXTERNALVCC 0x1
#define INVERTDISPLAY 0xA7
#define MEMORYMODE 0x20
#define NORMALDISPLAY 0xA6
#define PAGEADDR 0x22
#define SEGREMAP 0xA0
#define SETCOMPINS 0xDA
#define SETCONTRAST 0x81
#define SETDISPLAYCLOCKDIV 0xD5
#define SETDISPLAYOFFSET 0xD3
#define SETHIGHCOLUMN 0x10
#define SETLOWCOLUMN 0x00
#define SETMULTIPLEX 0xA8
#define SETPRECHARGE 0xD9
#define SETSEGMENTREMAP 0xA1
#define SETSTARTLINE 0x40
#define SETVCOMDETECT 0xDB
#define SWITCHCAPVCC 0x2

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) \
  {                         \
    int16_t t = a;          \
    a = b;                  \
    b = t;                  \
  }
#endif

enum class OLEDDISPLAY_COLOR
{
  BLACK = 0,
  WHITE = 1,
  INVERSE = 2
};

enum class OLEDDISPLAY_TEXT_ALIGNMENT
{
  TEXT_ALIGN_LEFT = 0,
  TEXT_ALIGN_RIGHT = 1,
  TEXT_ALIGN_CENTER = 2,
  TEXT_ALIGN_CENTER_BOTH = 3
};

enum class OLEDDISPLAY_GEOMETRY
{
  GEOMETRY_128_64 = 0,
  GEOMETRY_128_32 = 1,
  GEOMETRY_64_48 = 2,
  GEOMETRY_RAWMODE = 4
};

typedef char (*FontTableLookupFunction)(const uint8_t ch);
char DefaultFontTableLookup(const uint8_t ch);

/**
 * @brief create an oled display
 * 
 */
class OLEDDisplay
{
public:
  /**
   * @brief Construct a new OLEDDisplay object
   * 
   */
  OLEDDisplay();
  /**
   * @brief Destroy the OLEDDisplay object
   * 
   */
  virtual ~OLEDDisplay();

  /**
   * @brief get display width
   * 
   * @return uint16_t display width in pixels
   */
  uint16_t width(void) const { return displayWidth; };
  /**
   * @brief get display height
   * 
   * @return uint16_t display height in pixels
   */
  uint16_t height(void) const { return displayHeight; };

  /**
   * @brief use to resume after a deep sleep without resetting the display (what init() would do)
   * 
   * @return true connection to display established
   * @return false connection to display failed
   */
  bool allocateBuffer();

  /**
   * @brief allocates buffer and initializes driver and display
   * 
   * @return true success
   * @return false failed
   */
  bool init();

  /**
   * @brief free the memory used by the display
   * 
   */
  void end();

  /**
   * @brief cycle through the display
   * 
   */
  void resetDisplay(void);

  /**
   * @brief Set the Color of all drawing operations
   * 
   * @param color color to use
   */
  void setColor(OLEDDISPLAY_COLOR color);

  /**
   * @brief Get the current Color
   * 
   * @return OLEDDISPLAY_COLOR current color
   */
  OLEDDISPLAY_COLOR getColor();

  /**
   * @brief draw pixel at location
   * 
   * @param x x location
   * @param y y location
   */
  void setPixel(int16_t x, int16_t y);

  /**
   * @brief draw pixel at given location and color
   * 
   * @param x x location
   * @param y y location
   * @param color color
   */
  void setPixelColor(int16_t x, int16_t y, OLEDDISPLAY_COLOR color);

  /**
   * @brief clear a pixel at given location
   * 
   * @param x x location
   * @param y y location
   */
  void clearPixel(int16_t x, int16_t y);

  /**
   * @brief draw line from position 0 to position 1
   * 
   * @param x0 x 0 location
   * @param y0 y 0 location
   * @param x1 x 1 location
   * @param y1 y 1 location
   */
  void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1);

  /**
   * @brief draw a border of rectangle at given location
   * 
   * @param x x location
   * @param y y location
   * @param width width of rectangle
   * @param height height of rectangle
   */
  void drawRect(int16_t x, int16_t y, int16_t width, int16_t height);

  /**
   * @brief draw a filled rectangle at location
   * 
   * @param x x location
   * @param y y location
   * @param width width of rectangle
   * @param height height of rectangle
   */
  void fillRect(int16_t x, int16_t y, int16_t width, int16_t height);

  /**
   * @brief draw border of circle at location
   * 
   * @param x x location
   * @param y y location
   * @param radius circle radius
   */
  void drawCircle(int16_t x, int16_t y, int16_t radius);

  /**
   * @brief draw quadrants specified in quads bit mask
   * 
   * @param x0 x location
   * @param y0 y location
   * @param radius circle radius
   * @param quads quads bitmask to draw
   */
  void drawCircleQuads(int16_t x0, int16_t y0, int16_t radius, uint8_t quads);

  /**
   * @brief draw filled circle at location
   * 
   * @param x x location
   * @param y y location
   * @param radius radius of circle
   */
  void fillCircle(int16_t x, int16_t y, int16_t radius);

  /**
   * @brief draw horizontal line at x, y location
   * 
   * @param x x location to start line at
   * @param y y location to start line at
   * @param length length of the line
   */
  void drawHorizontalLine(int16_t x, int16_t y, int16_t length);

  /**
   * @brief draw vertical line at x,y location
   * 
   * @param x x location to start line at
   * @param y y location to start line at
   * @param length length of the line
   */
  void drawVerticalLine(int16_t x, int16_t y, int16_t length);

  /**
   * @brief draw a progress bar and fill it to specified amount
   * 
   * @param x x location to start at
   * @param y y location to start at
   * @param width width of bar
   * @param height height of bar
   * @param progress current progress in bar
   */
  void drawProgressBar(uint16_t x, uint16_t y, uint16_t width, uint16_t height, uint8_t progress);

  /**
   * @brief draw a fast image
   * 
   * @param x x location
   * @param y y location
   * @param width width of image
   * @param height height of image
   * @param image array of image bytes
   */
  void drawFastImage(int16_t x, int16_t y, int16_t width, int16_t height, const uint8_t *image);

  /**
   * @brief draw an xbm
   * 
   * @param x x location
   * @param y y location
   * @param width width of xbm
   * @param height height of xbm
   * @param xbm pointer to xbm bytes
   */
  void drawXbm(int16_t x, int16_t y, int16_t width, int16_t height, const uint8_t *xbm);

  /**
   * @brief draw an icon 16x16 xbm format
   * 
   * @param x x location
   * @param y y location
   * @param ico icon bytes 
   * @param inverse invert bytes
   */
  void drawIco16x16(int16_t x, int16_t y, const char *ico, bool inverse = false);

  /* Text functions */

  /**
   * @brief draw a string at given location
   * 
   * @param x x location
   * @param y y location
   * @param text text to draw
   */
  void drawString(int16_t x, int16_t y, std::string text);

  /**
   * @brief draw a formatted string at location (printf())
   * 
   * @param x x location
   * @param y y location
   * @param buffer buffer for string
   * @param format string format
   * @param ... arguments
   */
  void drawStringf(int16_t x, int16_t y, char *buffer, std::string format, ...);

  /**
   * @brief draw string with maximum width at given location, text will be wrapped when needed
   * 
   * @param x x location
   * @param y y location
   * @param maxLineWidth max string width 
   * @param text text to draw
   */
  void drawStringMaxWidth(int16_t x, int16_t y, uint16_t maxLineWidth, std::string text);

  /**
   * @brief Get the String Width in pixels of string
   * 
   * @param text string to get witdh for
   * @param length length of the string
   * @return uint16_t string width in pixels
   */
  uint16_t getStringWidth(const char *text, uint16_t length);

  /**
   * @brief Get the String Width for string
   * 
   * @param text string to get width for
   * @return uint16_t string width in bytes
   */
  uint16_t getStringWidth(std::string text);

  /**
   * @brief Set the Text Alignment
   * 
   * @param textAlignment  how text should be aligned
   */
  void setTextAlignment(OLEDDISPLAY_TEXT_ALIGNMENT textAlignment);

  // Sets the current font. Available default fonts
  // ArialMT_Plain_10, ArialMT_Plain_16, ArialMT_Plain_24
  /**
   * @brief Set the Font, available default fonts ArialMT_Plain_10, ArialMT_Plain_16, ArialMT_Plain_24,
   * 
   * @param fontData font to use for text
   */
  void setFont(const uint8_t *fontData);

  /**
   * @brief Set the function that converts utf-8 to font table
   * 
   * @param function function to use
   */
  void setFontTableLookupFunction(FontTableLookupFunction function);

  /* Display functions */

  /**
   * @brief turn display on
   * 
   */
  void displayOn(void);

  /**
   * @brief turn display off
   * 
   */
  void displayOff(void);

  /**
   * @brief invert display
   * 
   */
  void invertDisplay(void);

  /**
   * @brief set display to normal mode
   * 
   */
  void normalDisplay(void);

  // Set display contrast
  // 
  // 

  /**
   * @brief Set the display contrast
   *  really low brightness & contrast: contrast = 10, precharge = 5, comdetect = 0
   *  normal brightness & contrast:  contrast = 100
   * @param contrast contrast to use
   * @param precharge todo: figure out what this does
   * @param comdetect todo: figure out what this does
   */
  void setContrast(uint8_t contrast, uint8_t precharge = 241, uint8_t comdetect = 64);

  /**
   * @brief Set the Brightness
   * 
   * @param brightness brightness to use
   */
  void setBrightness(uint8_t brightness);

  /**
   * @brief reset display orientation and or mirroring
   * 
   */
  void resetOrientation();

  /**
   * @brief turn display upside down
   * 
   */
  void flipScreenVertically();

  /**
   * @brief mirror the display (for us in projectors) 
   * 
   */
  void mirrorScreen();

  /**
   * @brief write buffer to display memory (display the display)
   * 
   */
  virtual void display(void) = 0;

  /**
   * @brief clear local display buffer
   * 
   */
  void clear(void);

  // Log buffer implementation

  

  /**
   * @brief log buffer implementation
   * This will define the lines and characters you can
   * print to the screen. When you exeed the buffer size (lines * chars)
   * the output may be truncated due to the size constraint.
   * @param lines amount of lines
   * @param chars amount of chars per line
   * @return true success
   * @return false failure
   */
  bool setLogBuffer(uint16_t lines, uint16_t chars);

  /**
   * @brief draw the log buffer at x,y location
   * 
   * @param x x location
   * @param y y location
   */
  void drawLogBuffer(uint16_t x, uint16_t y);

  /**
   * @brief Get screen width
   * 
   * @return uint16_t screen width
   */
  uint16_t getWidth(void);

  /**
   * @brief Get screen height
   * 
   * @return uint16_t screen height
   */
  uint16_t getHeight(void);

  // Implement needed function to be compatible with Print class
  size_t write(uint8_t c);
  size_t write(const char *s);

  // Implement needed function to be compatible with Stream class
  int _putc(int c);
  int _getc() { return -1; };

  uint8_t *buffer;

#ifdef OLEDDISPLAY_DOUBLE_BUFFER
  uint8_t *buffer_back;
#endif

protected:
  OLEDDISPLAY_GEOMETRY geometry;

  uint16_t displayWidth;
  uint16_t displayHeight;
  uint16_t displayBufferSize;

  // Set the correct height, width and buffer for the geometry
  /**
   * @brief Set the Geometry of the display
   * 
   * @param g geometry
   * @param width display width
   * @param height display height
   */
  void setGeometry(OLEDDISPLAY_GEOMETRY g, uint16_t width = 0, uint16_t height = 0);

  OLEDDISPLAY_TEXT_ALIGNMENT textAlignment; //!< text alignment
  OLEDDISPLAY_COLOR color; //!< display color

  const uint8_t *fontData; //!< font to use

  // State values for logBuffer
  uint16_t logBufferSize; //!< log buffer size
  uint16_t logBufferFilled; //!< log buffer filled
  uint16_t logBufferLine; //!< log buffer current line
  uint16_t logBufferMaxLines; //!< log buffer maximum lines
  char *logBuffer; //!< log buffer

  // the header size of the buffer used, e.g. for the SPI command header
  virtual int getBufferOffset(void) = 0;

  // Send a command to the display (low level function)
  virtual void sendCommand(uint8_t com) { (void)com; };

  // Connect to the display
  virtual bool connect() { return false; };

  // Send all the init commands
  void sendInitCommands();

  // converts utf8 characters to extended ascii
  char *utf8ascii(std::string s);

  void inline drawInternal(int16_t xMove, int16_t yMove, int16_t width, int16_t height, const uint8_t *data, uint16_t offset, uint16_t bytesInData);

  void drawStringInternal(int16_t xMove, int16_t yMove, char *text, uint16_t textLength, uint16_t textWidth);

  FontTableLookupFunction fontTableLookupFunction;
};

#endif
