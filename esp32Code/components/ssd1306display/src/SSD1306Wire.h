/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 by ThingPulse, Daniel Eichhorn
 * Copyright (c) 2018 by Fabrice Weinberg
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ThingPulse invests considerable time and money to develop these open source libraries.
 * Please support us by buying our products (and not the clones) from
 * https://thingpulse.com
 *
 */

#ifndef SSD1306Wire_h
#define SSD1306Wire_h

#include "OLEDDisplay.h"
#include <algorithm>

#include "../../Delay/src/IDelay.h"
#include "../../i2c/src/II2c.h"
#include "../../dynamicarray/src/dynamicArray.h"

//--------------------------------------

class SSD1306Wire : public OLEDDisplay
{
private:
  uint8_t _address;
  IDelay *delay = nullptr;
  II2C *i2c = nullptr;
  Gpio_t rstPin = (Gpio_t)-1;
  IGpio* gpioInstance = nullptr;
public:
  /**
   * @brief Construct a new SSD1306Wire object
   * 
   */
  SSD1306Wire()
  {
  }

  /**
   * @brief initialize display
   * 
   * @param _address display i2c address
   * @param I2c i2c instance
   * @param Delay delay instance
   * @param GpioInstance gpio instance (can be nullptr if reset line is not connected)
   * @param rstPin reset pin (can be -1 if not connected)
   * @param g display geometry
   */
  void Init(uint8_t _address, II2C *I2c, IDelay *Delay, IGpio* GpioInstance, Gpio_t rstPin =(Gpio_t)-1, OLEDDISPLAY_GEOMETRY g = OLEDDISPLAY_GEOMETRY::GEOMETRY_128_64)
  {
    setGeometry(g);
    this->i2c = I2c;
    this->delay = Delay;
    this->_address = _address;
    this->rstPin = rstPin;
    this->gpioInstance = GpioInstance;
    OLEDDisplay::init();
  }

  bool connect()
  {
    if(gpioInstance != nullptr && rstPin != (Gpio_t)-1)
    {
      gpioInstance->WritePin(rstPin,0);
      delay->ms(50);
      gpioInstance->WritePin(rstPin,1);
    }
    return true;
  }

  void display(void)
  {
    const int x_offset = (128 - this->width()) / 2;
#ifdef OLEDDISPLAY_DOUBLE_BUFFER
    uint8_t minBoundY = UINT8_MAX;
    uint8_t maxBoundY = 0;

    uint8_t minBoundX = UINT8_MAX;
    uint8_t maxBoundX = 0;
    uint8_t x, y;

    // Calculate the Y bounding box of changes
    // and copy buffer[pos] to buffer_back[pos];
    for (y = 0; y < (this->height() / 8); y++)
    {
      for (x = 0; x < this->width(); x++)
      {
        uint16_t pos = x + y * this->width();
        if (buffer[pos] != buffer_back[pos])
        {
          minBoundY = std::min(minBoundY, y);
          maxBoundY = std::max(maxBoundY, y);
          minBoundX = std::min(minBoundX, x);
          maxBoundX = std::max(maxBoundX, x);
        }
        buffer_back[pos] = buffer[pos];
      }
      delay->ms(1);
    }

    // If the minBoundY wasn't updated
    // we can savely assume that buffer_back[pos] == buffer[pos]
    // holdes true for all values of pos

    if (minBoundY == UINT8_MAX)
      return;

    sendCommand(COLUMNADDR);
    sendCommand(x_offset + minBoundX);
    sendCommand(x_offset + maxBoundX);

    sendCommand(PAGEADDR);
    sendCommand(minBoundY);
    sendCommand(maxBoundY);

    uint8_t k = 0;
    DynamicArray<uint8_t> wireBuf = DynamicArray<uint8_t>();
    for (y = minBoundY; y <= maxBoundY; y++)
    {
      for (x = minBoundX; x <= maxBoundX; x++)
      {
        if (k == 0)
        {
          wireBuf.push_back(0x40);
        }

        wireBuf.push_back(buffer[x + y * this->width()]);
        k++;
        if (k == 16)
        {
          i2c->Transmit(_address, wireBuf.getHead(), wireBuf.size(), 0);
          wireBuf.clear();
          k = 0;
        }
      }
      delay->ms(1);
    }

    if (k != 0)
    {
      i2c->Transmit(_address, wireBuf.getHead(), wireBuf.size(), 0);
      wireBuf.clear();
    }
#else

    sendCommand(COLUMNADDR);
    sendCommand(x_offset);
    sendCommand(x_offset + (this->width() - 1));

    sendCommand(PAGEADDR);
    sendCommand(0x0);
    DynamicArray<uint8_t> wireBuf = DynamicArray<uint8_t>();
    for (uint16_t i = 0; i < displayBufferSize; i++)
    {
      wireBuf.push_back(0x40);
      for (uint8_t x = 0; x < 16; x++)
      {
        wireBuf.push_back(buffer[i]);
        i++;
      }
      i--;
      i2c->Transmit(_address, wireBuf.getHead(), wireBuf.size(), 0);
      wireBuf.clear();
    }
#endif
  }

private:
  int getBufferOffset(void)
  {
    return 0;
  }
  inline void sendCommand(uint8_t command)
  {
    uint8_t buffer[] = {0x80, command};
    i2c->Transmit(_address, buffer, 2, 0);
  }
};

#endif
