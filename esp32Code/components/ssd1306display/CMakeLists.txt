#space seperated list of string for c or cpp files to include in building the project
set(srcs "src/OLEDDisplay.cpp" "src/OLEDDisplayUI.cpp")
set(include_dirs "." "src/.")
set(require "i2c" "SPI" "Delay" "esp_timer")

idf_component_register(SRCS "${srcs}"
                    INCLUDE_DIRS "${include_dirs}"
                    REQUIRES "${require}")