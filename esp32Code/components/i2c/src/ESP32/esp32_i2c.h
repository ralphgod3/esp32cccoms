#ifdef __ESP32_ESP32__

#ifndef ESP32_I2C_H
#define ESP32_I2C_H

#include "../../../simpledebug/src/SimpleDebug.h"
#include "../../../Gpio/src/ESP32/Gpio.h"
#include "../II2c.h"
#include <driver/i2c.h>


class ESP32I2c : public II2C
{
private:
    DbgRegisterTag("ESP_I2C");
	bool m_IsInitialized = false; //!< indicates whether or not the instance is initialized
    i2c_port_t i2c_port;

public:
	ESP32I2c(){}
	~ESP32I2c(){}


    /**
     * @brief initialize i2c bus
     * 
     * @param i2c_port i2c port to use
     * @param Sda sda pin to use
     * @param Scl scl pin to use
     * @param pullupEnabled enable sda and scl pullups
     * @return StatusCode_t status code
     */
	StatusCode_t Init(i2c_port_t i2c_port, Gpio_t Sda, Gpio_t Scl, bool pullupEnabled = true);

    /**
     * @brief transmit an amount of data in blocking mode
     *
     * @param address addres we wish to transmit data to
     * @param pData pointer to data buffer
     * @param Size amount of bytes to transmit
     * @param Timeout time before operation times out
     * @return StatusCode_t status code
     */
    StatusCode_t Transmit(uint8_t address, uint8_t* pData, uint16_t Size, uint32_t Timeout);

    /**
     * @brief receive an amount of data in blocking mode
     *
     * @address address we wish to receive data from
     * @param pData pointer to data buffer
     * @param Size amount of data to be received on return holds amount of data that was received
     * @param Timeout time before operation times out
     * @return StatusCode_t status code
     */
    StatusCode_t Receive(uint8_t address, uint8_t* pData, uint16_t* Size, uint32_t Timeout);

    /**
     * @brief scan i2c bus for addresses, will use verbose prints to list found addresses in hex format
     * 
     * @return StatusCode_t status code
     */
    StatusCode_t Scan();

};



#endif // !ARDUINO_I2C_H


#endif //__ESP32_ESP32__