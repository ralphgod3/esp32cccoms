#ifdef __ESP32_ESP32__

#include "Esp32_I2C.h"

#define CHECK_INITIALIZED() { if(!m_IsInitialized) { return StatusCode_t::NOT_INITIALIZED;}}

#define CheckEspReturn(ret) if(ret != ESP_OK) {return ConvertEspErrorToStatusCode(ret);}

StatusCode_t ESP32I2c::Init(i2c_port_t i2c_port, Gpio_t Sda, Gpio_t Scl, bool pullupEnabled)
{
	if (m_IsInitialized)
	{
		return StatusCode_t::ALREADY_INITIALIZED;
	}
	if(Sda == (Gpio_t) -1 || Scl == (Gpio_t)-1)
	{
		return StatusCode_t::INVALID_ARG;
	}

	this->i2c_port = i2c_port;
	PullMode_t pMode = PullMode_t::None;
	if(pullupEnabled)
	{
		pMode = PullMode_t::PullUp;
	}
	Gpio::SetGpioPinConfig(Sda, pMode, GpioMode_t::InputOutputOD);
	Gpio::SetGpioPinConfig(Scl, pMode, GpioMode_t::InputOutputOD);

	i2c_config_t config = i2c_config_t();
	config.mode = I2C_MODE_MASTER;
	config.sda_io_num = Gpio::Gpio_tTogpio_num_t(Sda);
	config.scl_io_num = Gpio::Gpio_tTogpio_num_t(Scl);
	config.sda_pullup_en = pullupEnabled;
	config.scl_pullup_en = pullupEnabled;
	config.master.clk_speed = 400000;
	ESP_ERROR_CHECK(i2c_param_config(i2c_port, &config));

	ESP_ERROR_CHECK(i2c_driver_install(i2c_port, i2c_mode_t::I2C_MODE_MASTER, 0,0,0));
	
	m_IsInitialized = true;
	Transmit(1, nullptr, 0, portMAX_DELAY);
	return StatusCode_t::OK;
}


StatusCode_t ESP32I2c::Transmit(uint8_t address, uint8_t* pData, uint16_t Size, uint32_t Timeout)
{
	CHECK_INITIALIZED();
	if (pData == nullptr && Size != 0)
	{
		return StatusCode_t::INVALID_ARG;
	}

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, true);
	if(Size != 0)
	{
    	i2c_master_write(cmd, pData, Size, true);
	}
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 1000 / portTICK_RATE_MS);
	CheckEspReturn(ret);
    i2c_cmd_link_delete(cmd);
	return StatusCode_t::OK;
}

StatusCode_t ESP32I2c::Receive(uint8_t address, uint8_t* pData, uint16_t* Size, uint32_t Timeout)
{
	CHECK_INITIALIZED();

	if (pData == nullptr || Size == nullptr || *Size == 0)
	{
		return StatusCode_t::INVALID_ARG;
	}

	if(Timeout == 0)
	{
		Timeout = portMAX_DELAY;
	}

	esp_err_t err = ESP_OK;
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	err = i2c_master_start(cmd);
	CheckEspReturn(err);
	err = i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_READ, true);
	CheckEspReturn(err);
	if (*Size > 1)
	{
        err = i2c_master_read(cmd, pData, *Size - 1, i2c_ack_type_t::I2C_MASTER_ACK);
		CheckEspReturn(err);
    }
	err = i2c_master_read_byte(cmd, pData + *Size - 1, i2c_ack_type_t::I2C_MASTER_LAST_NACK);
	CheckEspReturn(err);
	err = i2c_master_stop(cmd);
	CheckEspReturn(err);
	err = i2c_master_cmd_begin(i2c_port, cmd, Timeout);
	CheckEspReturn(err);
	i2c_cmd_link_delete(cmd);
	return StatusCode_t::OK;
}

StatusCode_t ESP32I2c::Scan()
{
	if(!m_IsInitialized)
	{
		return StatusCode_t::NOT_INITIALIZED;
	}
	DbgLogfV("scanning for i2c addresses\r\n");
	for (int address = 1; address < 127; address++)
	{
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		StatusCode_t error = Transmit(address, nullptr, 0, portMAX_DELAY);
		if (error == StatusCode_t::OK)
		{
			if(address < 16)
			{
				DbgLogfV("I2C device found at address 0x0%x\r\n", address);	
			}
			else
			{
				DbgLogfV("I2C device found at address 0x%x\r\n", address);
			}
		}
	}
	return StatusCode_t::OK;
}

#endif //__ESP32_ESP32__