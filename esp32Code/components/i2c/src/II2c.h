#ifndef __I_I2C_H__
#define __I_I2C_H__
#include <stdint.h>
#include "../../statuscodesandexceptions/src/StatusCodes.h"


class II2C
{
public:
	virtual ~II2C() = default;

    /**
     * @brief transmit an amount of data in blocking mode
     *
     * @param address addres we wish to transmit data to
     * @param pData pointer to data buffer
     * @param Size amount of bytes to transmit
     * @param Timeout time before operation times out
     */
    virtual StatusCode_t Transmit(uint8_t address, uint8_t* pData, uint16_t Size, uint32_t Timeout) = 0;

    /**
     * @brief receive an amount of data in blocking mode
     *
     * @address address we wish to receive data from
     * @param pData pointer to data buffer
     * @param Size amount of data to be received on return holds amount of data that was received
     * @param Timeout time before operation times out
     */
    virtual StatusCode_t Receive(uint8_t address, uint8_t* pData, uint16_t* Size, uint32_t Timeout) = 0;

};


#endif