#ifndef __LIST_H__
#define __LIST_H__

#include <stdint.h>
#include "../../statuscodesandexceptions/src/StatusCodes.h"

/**
 * @brief internal node class for list
 * 
 * @tparam T whatever you want to store
 */
template <class T>
class ListInternalNode
{
public:
    /**
     * @brief data for this node
     * 
     */
    T Data = T();

    /**
     * @brief pointer to next value
     * 
     */
    ListInternalNode<T> *Next = nullptr;

    /**
     * @brief Construct a new node
     * 
     * @param data data the node will hold
     * @param next pointer to next node in list
     */
    ListInternalNode(T data, ListInternalNode<T> *next)
        : Data(data), Next(next)
    {
    }

    /**
     * @brief Construct a new node
     * 
     * @param data data this node will hold
     */
    ListInternalNode(T data) : ListInternalNode(data, nullptr)
    {
    }

    /**
     * @brief Destroy a node
     * 
     */
    ~ListInternalNode(){};
};

/**
 * @brief generic template implementation for a single linked list
 * 
 * @tparam T whatever you wish to store
 */
template <class T>
class List
{
private:
    ListInternalNode<T> *Head = nullptr;
    uint32_t m_Count = 0;

    int32_t findInternal(T toMatch, uint32_t startIndex = 0)
    {
        for (uint32_t i = startIndex; i < this->size(); i++)
        {
            if (this->operator[](i) == toMatch)
            {
                return i;
            }
        }
        return -1;
    }

    int32_t findInternal(bool (*MatchingFunction)(T obj1, T obj2), T toMatch, uint32_t startIndex = 0)
    {
        for (uint32_t i = startIndex; i < this->size(); i++)
        {
            if (MatchingFunction(toMatch, this->operator[](i)))
            {
                return i;
            }
        }
        return -1;
    }

    List<T> &internalCopy(List<T> &other)
    {
        if (this == &other)
        {
            return *this;
        }
        this->clear();

        this->m_Count = other.m_Count;

        if (other.Head != nullptr)
        {
            this->Head = new ListInternalNode<T>(other.Head->Data);

            ListInternalNode<T> *cur = Head;

            for (ListInternalNode<T> *cp_node = other.Head->Next; cp_node != nullptr; cp_node = cp_node->Next)
            {
                cur->Next = new ListInternalNode<T>(cp_node->Data);
                cur = cur->Next;
            }
        }

        return *this;
    }

public:
    /**
     * @brief Construct a new List object
     * 
     */
    List()
    {
        m_Count = 0;
        Head = nullptr;
    }

    /**
     * @brief Construct a new List object
     * 
     * @param other object to copy
     */
    List(const List &other) // copy constructor
    {
        internalCopy(const_cast<List<T> &>(other));
    }

    /**
     * @brief Construct a new List object
     * 
     * @param other object to copy
     */
    List(List &&other) // copy constructor
    {
        internalCopy(other);
    }

    /**
     * @brief copy assign function
     * 
     * @param other object to copy
     * @return List& this
     */
    List &operator=(const List &other) //copy assign
    {
        return internalCopy(const_cast<List<T> &>(other));
    }

    /**
     * @brief move assign operator
     * 
     * @param other object to move
     * @return List& this
     */
    List &operator=(List &&other) //move assign
    {
        return internalCopy(other);
    }

    /**
     * @brief Destroy the List object
     * 
     */
    ~List()
    {
        // if list is empty just delete it
        clear();
        m_Count = 0;
    }

    /**
     * @brief add elements to list at the back
     * 
     * @param data data for elements
     * @param size size of data to add
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_back(T *data, uint32_t size)
    {
        return insert(m_Count, data, size);
    }

    /**
     * @brief add element to list at the back
     * 
     * @param data data for element
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_back(T data)
    {
        return push_back(&data, 1);
    }

    /**
     * @brief add element to list at the front
     * 
     * @param data data for elements
     * @param size size of the data
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_front(T *data, uint32_t size)
    {
        return insert(0, data, size);
    }

    /**
     * @brief add element to list at the front
     * 
     * @param data data for element
     * @return StatusCode_t error when it fails
     */
    StatusCode_t push_front(T data)
    {
        return push_front(&data, 1);
    }

    /**
     * @brief insert element(s) into list
     * 
     * @param index list index to insert element(s) into
     * @param data data to insert for the element(s)
     * @param size size of the data in number of elements
     * @return StatusCode_t status code
     */
    StatusCode_t insert(uint32_t index, T *data, uint32_t size)
    {
        if (index > m_Count)
        {
            return StatusCode_t::OUT_OF_RANGE;
        }
        if (size == 0 || data == nullptr)
        {
            return StatusCode_t::INVALID_ARG;
        }

        //create a list with nodes for new data
        ListInternalNode<T> *tempHead = new ListInternalNode<T>(data[0]);
        ListInternalNode<T> *tempTail = tempHead;
        for (uint32_t i = 0; i < size - 1; i++)
        {
            tempTail->Next = new ListInternalNode<T>(data[i + 1]);
            tempTail = tempTail->Next;
        }

        //handle inserting at the head
        if (index == 0)
        {
            tempTail->Next = Head;
            Head = tempHead;
        }
        //handle inserting at tail
        else if (index == m_Count)
        {
            ListInternalNode<T> *ref = Head;
            while (ref->Next)
            {
                ref = ref->Next;
            }

            ref->Next = tempHead;
        }
        //handle inserting everywhere else
        else
        {
            ListInternalNode<T> *ref = Head;
            for (uint32_t i = 0; i < index - 1; i++)
            {
                ref = ref->Next;
            }

            ListInternalNode<T> *next = ref->Next;
            ref->Next = tempHead;
            tempTail->Next = next;
        }
        m_Count += size;
        return StatusCode_t::OK;
    }

    /**
     * @brief insert an element into the list
     * 
     * @param index index to insert element at
     * @param data element to insert
     * @return StatusCode_t OK or Error
     */
    StatusCode_t insert(uint32_t index, T data)
    {
        return insert(index, &data, 1);
    }

    /**
     * @brief remove element from list
     * 
     * @param index index to remove
     * @return StatusCode_t OK or Error
     */
    StatusCode_t erase(uint32_t index)
    {
        return erase(index, 1);
    }

    /**
     * @brief remove elements from list
     * 
     * @param index index to start removing
     * @param size amount of elements to removed
     * @return StatusCode_t OK or Error
     */
    StatusCode_t erase(uint32_t index, uint32_t size)
    {
        if (index > m_Count - 1)
        {
            return StatusCode_t::OUT_OF_RANGE;
        }
        if (index + size > m_Count)
        {
            size = (m_Count - 1) - index;
        }

        ListInternalNode<T> *lastRemaining = Head;
        ListInternalNode<T> *deleteStart = Head;
        //go to start index
        for (uint32_t i = 0; i < index; i++)
        {
            lastRemaining = deleteStart;
            deleteStart = deleteStart->Next;
        }

        //start removing elements
        ListInternalNode<T> *temp = deleteStart;
        ListInternalNode<T> *lastTemp = temp;
        for (uint32_t i = 0; i < size; i++)
        {
            lastTemp = temp;
            temp = temp->Next;
            delete lastTemp;
        }

        //if head was removed put a new head in place
        if (index == 0)
        {
            Head = temp;
        }
        else
        {
            lastRemaining->Next = temp;
        }
        m_Count -= size;
        return StatusCode_t::OK;
    }

    /**
     * @brief removes elements from front of the list
     * 
     * @param size amount of elements to remove
     * @return ListError statuscode
     */
    StatusCode_t pop_front(uint32_t size)
    {
        return erase(0, size);
    }

    /**
     * @brief removes element from front of the list
     * 
     * @return ListError statuscode
     */
    StatusCode_t pop_front()
    {
        return pop_front(1);
    }

    /**
     * @brief removes elements from back of list
     * 
     * @param size amount of elements to remove
     * @return ListError statuscode 
     */
    StatusCode_t pop_back(uint32_t size)
    {
        return erase(m_Count - 1, size);
    }

    /**
     * @brief removes element from back of list
     * 
     * @return ListError statuscode 
     */
    StatusCode_t pop_back()
    {
        return pop_back(1);
    }

    /**
     * @brief return amount of elements in list
     * 
     * @return uint32_t amount of elements
     */
    uint32_t size()
    {
        return m_Count;
    }

    /**
     * @brief array operator overload
     * 
     * @param i index to acces
     * @return T& reference to element
     */
    T &operator[](uint32_t i)
    {
        if (i == m_Count)
        {
            //push_back(T());
        }

        ListInternalNode<T> *Node = Head;
        for (uint32_t j = 0; j < i; j++)
        {
            Node = Node->Next;
        }
        return Node->Data;
    }

    /**
     * @brief erase the entire list
     * 
     * @return StatusCode_t statuscode
     */
    StatusCode_t clear()
    {
        if (Head == nullptr)
        {
            m_Count = 0;
            return StatusCode_t::OK;
        }
        return erase(0, size());
    }

    /**
     * @brief simple find for matching values, returns first matching index for value in list
     * 
     * @param toMatch object or value to match with
     * @return int32_t -1 on not found index of element otherwise
     */
    int32_t find(T toMatch)
    {
        return findInternal(toMatch);
    }

    /**
     * @brief finds all elements that have the matching value
     * 
     * @param toMatch value to match with
     * @return List<uint32_t> list of indexes with elements that match
     */
    List<uint32_t> findAll(T toMatch)
    {
        List<uint32_t> list = List<uint32_t>();
        int32_t idx = 0;
        while (idx >= 0)
        {
            idx = findInternal(toMatch, idx);
            if (idx >= 0)
            {
                list.push_back(idx);
                idx += 1;
            }
        }
        return list;
    }

    /**
     * @brief returns true if object is contained in list
     * 
     * @param toMatch object to match
     * @return true object is in list
     * @return false object is not in list
     */
    bool contains(T toMatch)
    {
        if (findInternal(toMatch) > -1)
        {
            return true;
        }
        return false;
    }

    /**
     * @brief find with costumizable match function
     * 
     * @param MatchingFunction function that specifies when contained object matches
     * @param toMatch object to match with
     * @return int32_t -1 on no match index of first matching object otherwise
     */
    int32_t find(bool (*MatchingFunction)(T obj1, T obj2), T toMatch)
    {
        return findInternal(MatchingFunction, toMatch);
    }

    /**
     * @brief find with costumizable match function
     * 
     * @param MatchingFunction function that specifies when contained object matches
     * @param toMatch object to match with
     * @return List<uint32_t> list of matching objects
     */
    List<uint32_t> findAll(bool (*MatchingFunction)(T obj1, T obj2), T toMatch)
    {
        List<uint32_t> list;
        int32_t idx = 0;
        while (idx >= 0)
        {
            idx = findInternal(MatchingFunction, toMatch, idx);
            if (idx >= 0)
            {
                list.push_back(idx);
                idx += 1;
            }
        }
        return list;
    }
};

#endif
