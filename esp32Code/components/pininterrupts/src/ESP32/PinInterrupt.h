#if defined(__ESP32_ESP32__) || defined(ESP32)

#ifndef __PIN_INTERRUPT_H__
#define __PIN_INTERRUPT_H__

#include <stdint.h>
#include "../../../Gpio/src/ESP32/Gpio.h"
#include "../IPinInterrupt.h"

/**
 * @brief simple way to attach and detach pin change interrupts, singleton
 * 
 */
class PinInterrupt final : public IPinInterrupt
{
private:
    bool initialized = false;
    
    //block copying of object
    PinInterrupt(const PinInterrupt&) = delete; // copy constructor
    PinInterrupt(PinInterrupt&&) = delete; // copy constructor
    PinInterrupt& operator=(const PinInterrupt&) = delete; //copy assign
    PinInterrupt& operator=(PinInterrupt&&) = delete; //move assign
protected:
    /**
     * @brief Construct a new Pin Interrupt object
     * 
     */
    PinInterrupt();

    /**
     * @brief Destroy the Pin Interrupt object
     * 
     */
    ~PinInterrupt();

public:

    /**
     * @brief Get the Instance of the PinInterrupt
     * 
     * @return PinInterrupt& instance
     */
    static PinInterrupt& GetInstance();

    /**
     * @brief attaches an interrupt to a pin, only 1 interrupt can be added to a pin
     * 
     * @param pin pin to attach interrupt at
     * @param pMode pull pin up down or no pull
     * @param interruptMode rising, falling, change
     * @param fnc function to call when interrupt is triggered
     * @param fncParam parameters to call with this function
     * @return StatusCode_t statuscode
     */
    StatusCode_t Attach(Gpio_t pin, PullMode_t pMode, InterruptMode_t interruptMode, void (*fnc)(void *), void *fncParam);

    /**
     * @brief detach an interrupt by pin
     * 
     * @param pin pin to detach
     * @return StatusCode_t  statuscode
     */
    StatusCode_t Detach(Gpio_t pin);


};
#endif // !_PININTERRUPT_H

#endif