#if defined(__ESP32_ESP32__) || defined(ESP32)

#include "PinInterrupt.h"
#include "driver/gpio.h"

/****************************************************************************
Public Functions
****************************************************************************/

PinInterrupt &PinInterrupt::GetInstance()
{
	static PinInterrupt s_PinInterrupt;
	return s_PinInterrupt;
}

StatusCode_t PinInterrupt::Attach(Gpio_t pin, PullMode_t pMode, InterruptMode_t interruptMode, void (*fnc)(void *), void *fncParam)
{
	if(!initialized)
	{
		gpio_install_isr_service(0);
		initialized = true;
	}

	gpio_num_t gpioPin = (gpio_num_t)pin;

    gpio_config_t config = gpio_config_t();
    config.intr_type = gpio_int_type_t::GPIO_INTR_DISABLE;
    config.mode = gpio_mode_t::GPIO_MODE_INPUT;
    config.pull_down_en = gpio_pulldown_t::GPIO_PULLDOWN_DISABLE;
    config.pull_up_en = gpio_pullup_t::GPIO_PULLUP_DISABLE;
    config.pin_bit_mask = (1ull << gpioPin);

    switch (pMode)
    {
    case PullMode_t::PullDown:
        config.pull_down_en = gpio_pulldown_t::GPIO_PULLDOWN_ENABLE;
        break;
    case PullMode_t::PullUp:
        config.pull_up_en = gpio_pullup_t::GPIO_PULLUP_ENABLE;
        break;
    default:
        
        break;
    }

	switch(interruptMode)
	{
		case InterruptMode_t::INTR_RISING :
		config.intr_type = gpio_int_type_t::GPIO_INTR_POSEDGE;
		break;
		case InterruptMode_t::INTR_FALLING :
		config.intr_type = gpio_int_type_t::GPIO_INTR_NEGEDGE;
		break;
		case InterruptMode_t::INTR_CHANGE :
		config.intr_type = gpio_int_type_t::GPIO_INTR_ANYEDGE;
		break;
		default:
			config.intr_type = gpio_int_type_t::GPIO_INTR_ANYEDGE;
		break;
	}

	esp_err_t err = gpio_config(&config);
    ESP_ERROR_CHECK(err);

	err = gpio_isr_handler_add(gpioPin, fnc, fncParam);
	ESP_ERROR_CHECK(err);
	
	return StatusCode_t::OK;
}

StatusCode_t PinInterrupt::Detach(Gpio_t pin)
{
	if(pin == Gpio_t::NUM_NC)
	{
		return StatusCode_t::INVALID_ARG;
	}
	gpio_num_t gpioPin = (gpio_num_t)pin;

	gpio_set_intr_type(gpioPin, gpio_int_type_t::GPIO_INTR_DISABLE);
	gpio_isr_handler_remove(gpioPin);
	return StatusCode_t::OK;
}

/****************************************************************************
Protected Functions
****************************************************************************/

PinInterrupt::PinInterrupt()
{
}

PinInterrupt::~PinInterrupt()
{
	gpio_uninstall_isr_service();
}

/****************************************************************************
private Functions
****************************************************************************/

#endif