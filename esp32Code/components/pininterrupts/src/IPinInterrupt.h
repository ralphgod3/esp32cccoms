#ifndef __I_PIN_INTERRUPT_H__
#define __I_PIN_INTERRUPT_H__

#include "../../statuscodesandexceptions/src/StatusCodes.h"
#include <stdint.h>
#include "../../Gpio/src/IGpio.h"

/**
 * @brief mode that the interrupt will operate in
 * 
 */
enum class InterruptMode_t
{
    INTR_RISING, /*!< interrupt is triggered when signal goes high */
    INTR_FALLING, /*!< interrupt is triggered when pinstate goes low*/
    INTR_CHANGE, /*!< interrupt is triggered when pinstate changes*/
    INTR_NONE /*!<used internally */
};

/**
 * @brief simple way to attach and detach pin change interrupts, singleton
 * 
 */
class IPinInterrupt
{
private:
    //block copying of object
    //IPinInterrupt(const IPinInterrupt&) = delete; // copy constructor
    //IPinInterrupt(IPinInterrupt&&) = delete; // copy constructor
    //IPinInterrupt& operator=(const IPinInterrupt&) = delete; //copy assign
    //IPinInterrupt& operator=(IPinInterrupt&&) = delete; //move assign
public:
    /**
     * @brief attaches an interrupt to a pin, only 1 interrupt can be added to a pin
     * 
     * @param pin pin to attach interrupt at
     * @param pMode pull pin up down or no pull
     * @param interruptMode rising, falling, change
     * @param fnc function to call when interrupt is triggered
     * @param fncParam parameters to call with this function
     * @return StatusCode_t statuscode
     */
    virtual StatusCode_t Attach(Gpio_t pin, PullMode_t pMode, InterruptMode_t interruptMode, void (*fnc)(void *), void *fncParam) = 0;

    /**
     * @brief detach an interrupt by pin
     * 
     * @param pin pin to detach
     * @return StatusCode_t  statuscode
     */
    virtual StatusCode_t Detach(Gpio_t pin) = 0;


};
#endif // !_IPININTERRUPT_H
