#if defined(__ESP32_ESP32__) || defined(ESP32)

#ifndef __ABSTRACTED_DELAY_H__
#define __ABSTRACTED_DELAY_H__


#include <stdint.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#ifdef __ESP32_ESP32__
#include "esp32/rom/ets_sys.h"
#endif
#include "../IDelay.h"

/**
 * @brief delay class, functions in this can be used staticly
 * 
 */
class Delay : public IDelay
{
public:

    /**
     * @brief Construct a new Delay object.
     * 
     */
    Delay() {};

    /**
     * @brief Destroy the Delay object
     * 
     */
    ~Delay() {};

    /**
     * @brief blocking delay of ms
     * 
     * @param ms milliseconds to delay
     */
    void ms(uint32_t ms)
    {
        vTaskDelay(ms/ portTICK_PERIOD_MS);
    }

    /**
     * @brief blocking delay of micro seconds
     *
     * @param us micro seconds to delay
     */
    void us(uint32_t us)
    {
        ets_delay_us(us);
    }

    /**
     * @brief blocking delay of seconds
     * 
     * @param s seconds to delay
     */
    void s(float s)
    {
        ms(s * 1000.0f);
    }

    /**
     * @brief returns amount of milliseconds since device started
     * @return uint64_t milliseconds since startup
     */
    uint64_t GetMillisSinceStartup()
    {
        return esp_timer_get_time()/1000;
    }
};
#endif
#endif
