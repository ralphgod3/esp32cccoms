#ifndef INTERFACE_DELAY_H
#define INTERFACE_DELAY_H
#include <stdint.h>

/**
 * @brief interface for simple blocking delay class
 * 
 */
class IDelay
{
public:

    /**
     * @brief Destroy the IDelay object
     * 
     */
    virtual ~IDelay() = default;

    /**
     * @brief blocking delay of ms
     * 
     * @param ms milliseconds to delay
     */
    virtual void ms(uint32_t ms) = 0;

    /**
     * @brief blocking delay of seconds
     * 
     * @param s seconds to delay
     */
    virtual void s(float s) = 0;

    /**
     * @brief blocking delay of micro seconds
     *
     * @param us microseconds to delay
     */
    virtual void us(uint32_t us) = 0;


    /**
     * @brief returns amount of milliseconds since device started
     * @return uint64_t milliseconds since startup
     */
    virtual uint64_t GetMillisSinceStartup() = 0;
};
#endif

