#if defined(__DEBUG__) || defined(DEBUG) || defined(ARDUINO)

#include "SimpleDebug.h"
#include <stdlib.h>

void SimpleDebug::SetColor(PrintColor color, char* colorBuf)
{
	if (str == nullptr)
	{
		return;
	}
	switch (color)
	{
	case PrintColor::Red:
		strcpy(colorBuf, "\033[0;31m");
		break;
	case PrintColor::Green:
		strcpy(colorBuf, "\033[0;32m");
		break;
	case PrintColor::Yellow:
		strcpy(colorBuf, "\033[0;33m");
		break;
	case PrintColor::Blue:
		strcpy(colorBuf, "\033[0;34m");
		break;
	case PrintColor::Magenta:
		strcpy(colorBuf, "\033[0;35m");
		break;
	case PrintColor::Cyan:
		strcpy(colorBuf, "\033[0;36m");
		break;
	default:
		break;
	}
}

void SimpleDebug::ResetColor(char* resetBuf)
{
	strcpy(resetBuf, "\033[0m");
}

SimpleDebug::PrintColor SimpleDebug::DebugLevelToColor(DebugLevel level)
{
	PrintColor ret = PrintColor::Magenta;
	switch (level)
	{
	case DebugLevel::Error:
		ret = PrintColor::Red;
		break;
	case DebugLevel::Warning:
		ret = PrintColor::Yellow;
		break;
	case DebugLevel::Info:
		ret = PrintColor::Green;
		break;
	case DebugLevel::Debug:
		ret = PrintColor::Blue;
		break;
	case DebugLevel::Verbose:
		ret = PrintColor::Cyan;
		break;
	default:
		break;
	}
	return ret;
}

char SimpleDebug::GetDebugLevelPrefixChar(DebugLevel level)
{
	char ret = ' ';
	switch (level)
	{
	case DebugLevel::Error:
		ret = 'E';
		break;
	case DebugLevel::Warning:
		ret = 'W';
		break;
	case DebugLevel::Info:
		ret = 'I';
		break;
	case DebugLevel::Debug:
		ret = 'D';
		break;
	case DebugLevel::Verbose:
		ret = 'V';
		break;
	default:
		break;
	}
	return ret;
}

void SimpleDebug::SetGlobalLogLevel(DebugLevel level)
{
	globalLevel = level;
	for (uint32_t i = 0; i < p_Tags.size(); i++)
	{
		p_Tags[i].Dlevel = level;
	}
}

void SimpleDebug::SetLogLevelForTag(const char* Tag, DebugLevel level)
{
	TagInfo tempT = TagInfo(Tag);
	int32_t indexloc = p_Tags.find(tempT);
	if (indexloc == -1)
	{
		p_Tags.push_back(tempT);
		indexloc = p_Tags.size() - 1;
	}

	p_Tags[indexloc].Dlevel = level;
}

void SimpleDebug::Log(const char* Tag, DebugLevel Level, const char* format, ...)
{
	if (str == nullptr)
	{
		return;
	}

	TagInfo tempT = TagInfo(Tag);
	int32_t indexloc = p_Tags.find(tempT);
	if (indexloc == -1)
	{
		tempT.Dlevel = globalLevel;
		p_Tags.push_back(tempT);
		indexloc = p_Tags.size() - 1;
	}

	if ((int)Level <= (int)p_Tags[indexloc].Dlevel)
	{
		va_list args;

		va_start(args, format);
		int ret = vsnprintf((char*)NULL, 0, format, args);
		char* vacmd = new char[ret + 1];
		va_end(args);

		va_start(args, format);
		ret = vsnprintf(vacmd, ret + 1, format, args);
		if (ret != -1)
		{
			InternalLog(Tag, vacmd, Level);
		}
		delete[] vacmd;
		va_end(args);

	}
}

void SimpleDebug::InternalLog(const char* Tag, const char* inString, DebugLevel level)
{
	char* buf = new char[20 + strlen(Tag) + strlen(inString)];
	memset(buf, 0, 20 + strlen(Tag) + strlen(inString));

	uint16_t curLoc = 0;

	if (p_colorsEnabled)
	{
		SetColor(DebugLevelToColor(level), buf);
		curLoc += 7;
	}

	buf[curLoc++] = GetDebugLevelPrefixChar(level);
	buf[curLoc++] = ':';
	buf[curLoc++] = ' ';

	strcpy(&buf[curLoc], Tag);
	curLoc += strlen(Tag);
	buf[curLoc++] = ':';
	buf[curLoc++] = ' ';

	strcpy(&buf[curLoc], inString);
	curLoc += strlen(inString);
	if (p_colorsEnabled)
	{
		ResetColor(&buf[curLoc]);
		curLoc += 4;
	}

	str->Write(buf, curLoc);
	delete[] buf;
}

#endif