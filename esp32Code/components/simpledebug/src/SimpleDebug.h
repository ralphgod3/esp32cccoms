
/**
 * @brief simple debug class, initialize first by doing SimpleDebug::getInstance().Init();, after that macros can be used to log in colors
 * @brief ensure that compilation is done with the flag __DEBUG__ to enable the logging macros
 * 
 */

#if defined(__DEBUG__) || defined(DEBUG) || defined(ARDUINO)
#ifndef __SIMPLE_DEBUG_H__
#define __SIMPLE_DEBUG_H__

#include "../../mstream/src/ImStream.h"
#include "../../dynamicarray/src/dynamicArray.h"
#include <stddef.h>

/**
 * @brief initialize debug logging, accepts ImStream * and a bool
 * 
 */
#define DbgInit(stream) SimpleDebug::getInstance().Init(stream);

/**
 * @brief enable fancy colored debug printing
 * 
 */
#define DbgEnableColors() SimpleDebug::getInstance().SetColorsEnabled(true);
/**
 * @brief disabled colored debug printing
 * 
 */
#define DbgDisableColors() SimpleDebug::getInstance().SetColorsEnabled(false);

/**
 * @brief register a debug tag used for printing in debug lines, and disabling certain components from printing debug messages
 * 
 */
#define DbgRegisterTag(tag) static constexpr const char* DEBUG_TAG = tag

/**
 * @brief log a error
 * 
 */
#define DbgLogE(format) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Error, format);
/**
 * @brief log a warning
 * 
 */
#define DbgLogW(format) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Warning, format);
/**
 * @brief log a info message
 * 
 */
#define DbgLogI(format) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Info, format);
/**
 * @brief log a debug message
 * 
 */
#define DbgLogD(format) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Debug, format);
/**
 * @brief log a verbose message
 * 
 */
#define DbgLogV(format) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Verbose, format);

/**
 * @brief log a error
 * 
 */
#define DbgLogfE(format, ...) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Error, format, ##__VA_ARGS__);
/**
 * @brief log a warning
 * 
 */
#define DbgLogfW(format, ...) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Warning, format, ##__VA_ARGS__);
/**
 * @brief log a info message
 * 
 */
#define DbgLogfI(format, ...) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Info, format, ##__VA_ARGS__);
/**
 * @brief log a debug message
 * 
 */
#define DbgLogfD(format, ...) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Debug, format, ##__VA_ARGS__);
/**
 * @brief log a verbose message
 * 
 */
#define DbgLogfV(format, ...) SimpleDebug::getInstance().Log(DEBUG_TAG, DebugLevel::Verbose, format, ##__VA_ARGS__);

/**
 * @brief sets the global log level for all tags, overwrites any modiefied local tags
 * 
 */
#define DbgSetGlobalLogLevel(level) SimpleDebug::getInstance().SetGlobalLogLevel(level);

/**
 * @brief sets log level for a specific tag, overwrites the global level for this tag
 * 
 */
#define DbgSetLogLevel(Tag, level) SimpleDebug::getInstance().SetLogLevelForTag(Tag, level);

/**
 * @brief log levels
 * 
 */
enum class DebugLevel
{
    None = 0,    //!< no logs will be printed
    Error = 1,   //!< error logs will be printed
    Warning = 2, //!< error and warning logs will be printed
    Info = 3,    //!< error, warning and info logs will be printed
    Debug = 4,   //!< error, warning, info and Debug logs will be printed
    Verbose = 5, //!< error, warning, info, Debug and Verbose logs will be written
};

/**
 * @brief simple debug class, should be initialized in main with a stream to output logs towards, singleton so will work through enitre program once initialized,
 * it is preferred to use the debug macros to write logs since they are shorter
 * 
 */
class SimpleDebug
{
private:
    struct TagInfo
    {
        DynamicArray<char> tag = DynamicArray<char>();
        uint16_t tagLen = 0;
        DebugLevel Dlevel = DebugLevel::Error;

        TagInfo() {}
        TagInfo(const char *Tag)
        {
            tagLen = strlen(Tag);
            tag.clear();
            tag.push_back((char*)Tag, tagLen);
        }


        inline bool operator==(TagInfo &other)
        {
            if (other.tagLen != this->tagLen)
            {
                return false;
            }
            else if (memcmp(other.tag.getHead() , this->tag.getHead(), this->tagLen) == 0)
            {
                return true;
            }
            return false;
        }
    };

    // colors that can be printed
    enum class PrintColor
    {
        Red,
        Yellow,
        Green,
        Blue,
        Cyan,
        Magenta
    };

    ImStream *str = nullptr;
    DynamicArray<TagInfo> p_Tags = DynamicArray<TagInfo>();
    DebugLevel globalLevel = DebugLevel::Verbose;

    //construct and write the log message
    void InternalLog(const char *Tag, const char *inString, DebugLevel level);
    //grabs a color code and puts it into the character buffer, buffer should be of atleast size 8
    void SetColor(PrintColor color, char *colorBuf);
    //grabs the reset color code and puts it into character buffer, buffer should be of at least size 5
    void ResetColor(char *resetBuf);
    // convert a debug level to a color
    PrintColor DebugLevelToColor(DebugLevel level);
    //get the character that the log should be prefixed with depending on the debug level E,W,I,D,V
    char GetDebugLevelPrefixChar(DebugLevel level);
    //fancy colors enabled ?
    bool p_colorsEnabled = true;
protected:
    /**
     * @brief Construct a new Simple Debug object
     * 
     */
    SimpleDebug() {}
    /**
     * @brief Destroy the Simple Debug object
     * 
     */
    ~SimpleDebug(){}

public:
    /**
     * @brief Get the Instance of the debug class
     * 
     * @return SimpleDebug& debug instance
     */
    static SimpleDebug &getInstance()
    {
        static SimpleDebug s_Debug;
        return s_Debug;
    }

    /**
     * @brief initialize debug class, has to be done once preferably as soon as possible in the main to enable logging
     * 
     * @param InOutStream stream that will be used for printing logs
     * @return StatusCode_t status code
     */
    StatusCode_t Init(ImStream *InOutStream)
    {
        if (InOutStream == nullptr)
        {
            return StatusCode_t::INVALID_ARG;
        }
        str = InOutStream;
        return StatusCode_t::OK;
    }

    /**
     * @brief write a log to the stream
     * 
     * @param Tag tag the log should have
     * @param Level debuglevel of the written log
     * @param format format for the log
     * @param ... arguments
     */
    void Log(const char *Tag, DebugLevel Level, const char *format, ...);

    /**
     * @brief Set the Global Log Level for all tags
     * 
     * @param level log level to set
     */
    void SetGlobalLogLevel(DebugLevel level);

    /**
     * @brief Set the Log Level For a specific Tag
     * 
     * @param Tag tag to set log level for
     * @param level level the tag should be logged at
     */
    void SetLogLevelForTag(const char *Tag, DebugLevel level);

    /**
     * @brief set colors enabled, enables/ disables fancy colored text
     * 
     * @param enabled are colors enabled
     */
    void SetColorsEnabled(bool enabled)
    {
        p_colorsEnabled = enabled;
    }
};

#endif
#else
#define DbgRegisterTag(tag)
#define DbgEnableColors()
#define DbgDisableColors()
#define DbgInit(stream)
#define DbgLogE(format)
#define DbgLogW(format)
#define DbgLogI(format)
#define DbgLogD(format)
#define DbgLogV(format)
#define DbgLogfE(format, ...)
#define DbgLogfW(format, ...)
#define DbgLogfI(format, ...)
#define DbgLogfD(format, ...)
#define DbgLogfV(format, ...)
#define DbgSetGlobalLogLevel(level)
#define DbgSetLogLevel(Tag, level)
#endif