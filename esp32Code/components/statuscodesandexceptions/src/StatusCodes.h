#ifndef __STATUSCODES_H__
#define __STATUSCODES_H__

#ifdef ARDUINO
#include <Arduino.h>
#else
#include <stdint.h>
#include <string>
#endif

/**
 * @brief unified statuscode return for projects
 * 
 */
enum class StatusCode_t
{
	OK = 0,					/*!< everything is fine*/
	FAIL = -1,				/*!< generic error*/
	NOT_INITIALIZED = -2,	/*!< class not initialized*/
	ALREADY_INITIALIZED = -3,/*!< class already initialized*/
	NO_MEM = -4,			/*!< Out of memory */
	INVALID_ARG = -5,		/*!< Invalid argument */
	INVALID_STATE = -6,		/*!< Invalid state */
	INVALID_SIZE = -7,		/*!< Invalid size */
	INVALID_DATA = -8,		/*!< invalid internal data */
	NOT_FOUND = -9,			/*!< Requested resource not found */
	NOT_SUPPORTED = -10,	/*!< Operation or feature not supported */
	TIMEOUT = -11,			/*!< Operation timed out */
	INVALID_RESPONSE = -12,	/*!< Received response invalid */
	INVALID_CRC = -13,		/*!< CRC or checksum invalid */
	INVALID_VERSION = -14,	/*!< Version invalid */
	INVALID_MAC = -15,		/*!< MAC address invalid */
	BUFFER_OVERFLOW = -16,	/*!< internal buffer overflowed, increase buffer size */
	OUT_OF_RANGE = -17,		/*!< argument is out of range */
};

/**
 * @brief class that functions as a namespace for function that converts statuscode to text
 * 
 */
class SC
{
public:
	#ifdef ARDUINO
	/**
	 * @brief Get a text version of a statuscode
	 * 
	 * @param code statuscode to convert
	 * @return String message belonging to that statuscode
	 */
	static String GetMsgFromStatusCode(StatusCode_t code)
	{
		switch (code)
		{

		case StatusCode_t::OK:
			return F("everything is ok");
			break;
		case StatusCode_t::FAIL:
			return F("generic error");
			break;
		case StatusCode_t::NOT_INITIALIZED:
			return F("resource not initialized");
			break;
		case StatusCode_t::ALREADY_INITIALIZED:
			return F("resource already initialized");
			break;
		case StatusCode_t::NO_MEM:
			return F("out of memory");
			break;
		case StatusCode_t::INVALID_ARG:
			return F("invalid argument");
			break;
		case StatusCode_t::INVALID_STATE:
			return F("invalid state");
			break;
		case StatusCode_t::INVALID_SIZE:
			return F("invalid size");
			break;
		case StatusCode_t::INVALID_DATA:
			return F("invalid internal data");
			break;
		case StatusCode_t::NOT_FOUND:
			return F("Requested resource not found");
			break;
		case StatusCode_t::NOT_SUPPORTED:
			return F("Operation or feature not supported");
			break;
		case StatusCode_t::TIMEOUT:
			return F("Operation timed out");
			break;
		case StatusCode_t::INVALID_RESPONSE:
			return F("Received response was invalid");
			break;
		case StatusCode_t::INVALID_CRC:
			return F("CRC or checksum was invalid");
			break;
		case StatusCode_t::INVALID_VERSION:
			return F("Version was invalid");
			break;
		case StatusCode_t::INVALID_MAC:
			return F("MAC address was invalid");
			break;
		case StatusCode_t::BUFFER_OVERFLOW:
			return F("internal buffer overflowed, increase buffer size");
			break;
		default:
			return F("Invalid statuscode");
			break;
		}
	}
	#else
	/**
	 * @brief Get a text version of a statuscode
	 * 
	 * @param code statuscode to convert
	 * @return String message belonging to that statuscode
	 */
	static std::string GetMsgFromStatusCode(StatusCode_t code)
	{
		switch (code)
		{

		case StatusCode_t::OK:
			return ("everything is ok");
			break;
		case StatusCode_t::FAIL:
			return ("generic error");
			break;
		case StatusCode_t::NOT_INITIALIZED:
			return ("resource not initialized");
			break;
		case StatusCode_t::ALREADY_INITIALIZED:
			return ("resource already initialized");
			break;
		case StatusCode_t::NO_MEM:
			return ("out of memory");
			break;
		case StatusCode_t::INVALID_ARG:
			return ("invalid argument");
			break;
		case StatusCode_t::INVALID_STATE:
			return ("invalid state");
			break;
		case StatusCode_t::INVALID_SIZE:
			return ("invalid size");
			break;
		case StatusCode_t::INVALID_DATA:
			return ("invalid internal data");
			break;
		case StatusCode_t::NOT_FOUND:
			return ("Requested resource not found");
			break;
		case StatusCode_t::NOT_SUPPORTED:
			return ("Operation or feature not supported");
			break;
		case StatusCode_t::TIMEOUT:
			return ("Operation timed out");
			break;
		case StatusCode_t::INVALID_RESPONSE:
			return ("Received response was invalid");
			break;
		case StatusCode_t::INVALID_CRC:
			return ("CRC or checksum was invalid");
			break;
		case StatusCode_t::INVALID_VERSION:
			return ("Version was invalid");
			break;
		case StatusCode_t::INVALID_MAC:
			return ("MAC address was invalid");
			break;
		case StatusCode_t::BUFFER_OVERFLOW:
			return ("internal buffer overflowed, increase buffer size");
			break;
		default:
			return ("Invalid statuscode");
			break;
		}
	}
	#endif
};


#if defined(__ESP32_ESP32__) || defined(ESP32)
#include "esp_err.h"
/**
 * @brief converts an esp status code to a global statuscode
 * 
 * @param err esp status code
 * @return StatusCode_t global status code
 */
static StatusCode_t ConvertEspErrorToStatusCode(esp_err_t err)
{
	switch (err)
	{
	case ESP_OK:
		return StatusCode_t::OK;
		break;
	case ESP_FAIL:
		return StatusCode_t::FAIL;
		break;
	case ESP_ERR_NO_MEM:
		return StatusCode_t::NO_MEM;
		break;
	case ESP_ERR_INVALID_ARG:
		return StatusCode_t::INVALID_ARG;
		break;
	case ESP_ERR_INVALID_STATE:
		return StatusCode_t::INVALID_STATE;
		break;
	case ESP_ERR_INVALID_SIZE:
		return StatusCode_t::INVALID_SIZE;
		break;
	case ESP_ERR_NOT_FOUND:
		return StatusCode_t::NOT_FOUND;
		break;
	case ESP_ERR_NOT_SUPPORTED:
		return StatusCode_t::NOT_SUPPORTED;
		break;
	case ESP_ERR_TIMEOUT:
		return StatusCode_t::TIMEOUT;
		break;
	case ESP_ERR_INVALID_RESPONSE:
		return StatusCode_t::INVALID_RESPONSE;
		break;
	case ESP_ERR_INVALID_CRC:
		return StatusCode_t::INVALID_CRC;
		break;
	case ESP_ERR_INVALID_VERSION:
		return StatusCode_t::INVALID_VERSION;
		break;
	case ESP_ERR_INVALID_MAC:
		return StatusCode_t::INVALID_MAC;
		break;
	
	default:
		break;
	}
	return StatusCode_t::FAIL;
}

#endif

#endif