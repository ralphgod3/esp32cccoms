#if defined(__ESP32_ESP32__) || defined(ESP32)

#include "Gpio.h"

Gpio &Gpio::GetInstance()
{
	static Gpio s_Gpio;
	return s_Gpio;
}

StatusCode_t Gpio::SetPinMode(Gpio_t pin, PullMode_t pullMode, GpioMode_t mode)
{
    return SetGpioPinConfig(pin, pullMode, mode);
}

gpio_mode_t Gpio::ConvertEspModeToMode(GpioMode_t mode)
{
    switch (mode)
    {
    case GpioMode_t::Disable:
        return gpio_mode_t::GPIO_MODE_DISABLE;
        break;
    case GpioMode_t::Input:
        return gpio_mode_t::GPIO_MODE_INPUT;
        break;
    case GpioMode_t::Output:
        return gpio_mode_t::GPIO_MODE_OUTPUT;
        break;
    case GpioMode_t::OutputOD:
        return gpio_mode_t::GPIO_MODE_OUTPUT_OD;
        break;
    case GpioMode_t::InputOutputOD:
        return gpio_mode_t::GPIO_MODE_INPUT_OUTPUT_OD;
        break;
    case GpioMode_t::InputOutput:
        return gpio_mode_t::GPIO_MODE_INPUT_OUTPUT;
        break;
    }
    return gpio_mode_t::GPIO_MODE_DISABLE;
}

void Gpio::WritePin(Gpio_t pin, bool state)
{
    gpio_num_t tempPin = Gpio_tTogpio_num_t(pin);
    if (state == true)
    {
        gpio_set_level(tempPin, 1);
    }
    else
    {
        gpio_set_level(tempPin, 0);
    }
}

bool Gpio::readPin(Gpio_t pin)
{
    if (gpio_get_level(Gpio_tTogpio_num_t(pin)) == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
    return false;
}

gpio_num_t Gpio::Gpio_tTogpio_num_t(Gpio_t pin)
{
    switch (pin)
    {
    case Gpio_t::NUM_NC:
    #ifdef ARDUINO
        return (gpio_num_t) -1;
    #else
        return gpio_num_t::GPIO_NUM_NC;
    #endif
        break;
    case Gpio_t::NUM_0:
        return gpio_num_t::GPIO_NUM_0;
        break;
    case Gpio_t::NUM_1:
        return gpio_num_t::GPIO_NUM_1;
        break;
    case Gpio_t::NUM_2:
        return gpio_num_t::GPIO_NUM_2;
        break;
    case Gpio_t::NUM_3:
        return gpio_num_t::GPIO_NUM_3;
        break;
    case Gpio_t::NUM_4:
        return gpio_num_t::GPIO_NUM_4;
        break;
    case Gpio_t::NUM_5:
        return gpio_num_t::GPIO_NUM_5;
        break;
    case Gpio_t::NUM_6:
        return gpio_num_t::GPIO_NUM_6;
        break;
    case Gpio_t::NUM_7:
        return gpio_num_t::GPIO_NUM_7;
        break;
    case Gpio_t::NUM_8:
        return gpio_num_t::GPIO_NUM_8;
        break;
    case Gpio_t::NUM_9:
        return gpio_num_t::GPIO_NUM_9;
        break;
    case Gpio_t::NUM_10:
        return gpio_num_t::GPIO_NUM_10;
        break;
    case Gpio_t::NUM_11:
        return gpio_num_t::GPIO_NUM_11;
        break;
    case Gpio_t::NUM_12:
        return gpio_num_t::GPIO_NUM_12;
        break;
    case Gpio_t::NUM_13:
        return gpio_num_t::GPIO_NUM_13;
        break;
    case Gpio_t::NUM_14:
        return gpio_num_t::GPIO_NUM_14;
        break;
    case Gpio_t::NUM_15:
        return gpio_num_t::GPIO_NUM_15;
        break;
    case Gpio_t::NUM_16:
        return gpio_num_t::GPIO_NUM_16;
        break;
    case Gpio_t::NUM_17:
        return gpio_num_t::GPIO_NUM_17;
        break;
    case Gpio_t::NUM_18:
        return gpio_num_t::GPIO_NUM_18;
        break;
    case Gpio_t::NUM_19:
        return gpio_num_t::GPIO_NUM_19;
        break;
    case Gpio_t::NUM_20:
    #ifdef ARDUINO
        return (gpio_num_t) 20;
    #else
        return gpio_num_t::GPIO_NUM_20;
    #endif
        break;
    case Gpio_t::NUM_21:
        return gpio_num_t::GPIO_NUM_21;
        break;
    case Gpio_t::NUM_22:
        return gpio_num_t::GPIO_NUM_22;
        break;
    case Gpio_t::NUM_23:
        return gpio_num_t::GPIO_NUM_23;
        break;
    case Gpio_t::NUM_25:
        return gpio_num_t::GPIO_NUM_25;
        break;
    case Gpio_t::NUM_26:
        return gpio_num_t::GPIO_NUM_26;
        break;
    case Gpio_t::NUM_27:
        return gpio_num_t::GPIO_NUM_27;
        break;
    case Gpio_t::NUM_28:
    #ifdef ARDUINO
        return (gpio_num_t) 28;
    #else
        return gpio_num_t::GPIO_NUM_28;
    #endif
        break;
    case Gpio_t::NUM_29:
    #ifdef ARDUINO
        return (gpio_num_t) 29;
    #else
        return gpio_num_t::GPIO_NUM_29;
    #endif
        break;
    case Gpio_t::NUM_30:
    #ifdef ARDUINO
        return (gpio_num_t) 30;
    #else
        return gpio_num_t::GPIO_NUM_30;
    #endif
        break;
    case Gpio_t::NUM_31:
    #ifdef ARDUINO
        return (gpio_num_t) 31;
    #else
        return gpio_num_t::GPIO_NUM_31;
    #endif
        break;
    case Gpio_t::NUM_32:
        return gpio_num_t::GPIO_NUM_32;
        break;
    case Gpio_t::NUM_33:
        return gpio_num_t::GPIO_NUM_33;
        break;
    case Gpio_t::NUM_34:
        return gpio_num_t::GPIO_NUM_34;
        break;
    case Gpio_t::NUM_35:
        return gpio_num_t::GPIO_NUM_35;
        break;
    case Gpio_t::NUM_36:
        return gpio_num_t::GPIO_NUM_36;
        break;
    case Gpio_t::NUM_37:
        return gpio_num_t::GPIO_NUM_37;
        break;
    case Gpio_t::NUM_38:
        return gpio_num_t::GPIO_NUM_38;
        break;
    case Gpio_t::NUM_39:
        return gpio_num_t::GPIO_NUM_39;
        break;
    default:
        break;
    }
    #ifdef ARDUINO
        return (gpio_num_t) -1;
    #else
        return gpio_num_t::GPIO_NUM_NC;
    #endif
}

StatusCode_t Gpio::SetGpioPinConfig(Gpio_t pin, PullMode_t pullMode, GpioMode_t mode)
{

    gpio_num_t m_pin = Gpio::Gpio_tTogpio_num_t(pin);
    gpio_config_t config = gpio_config_t();
    config.intr_type = (gpio_int_type_t)GPIO_PIN_INTR_DISABLE;
    config.pull_down_en = gpio_pulldown_t::GPIO_PULLDOWN_DISABLE;
    config.pull_up_en = gpio_pullup_t::GPIO_PULLUP_DISABLE;
    config.pin_bit_mask = (1ull << m_pin);

    switch (mode)
    {
    case GpioMode_t::Input:
        config.mode = GPIO_MODE_INPUT;
        break;
    case GpioMode_t::Output:
        config.mode = GPIO_MODE_OUTPUT;
        break;
    case GpioMode_t::OutputOD:
        config.mode = GPIO_MODE_OUTPUT_OD;
        break;
    case GpioMode_t::InputOutputOD:
        config.mode = GPIO_MODE_INPUT_OUTPUT_OD;
        break;
    case GpioMode_t::InputOutput:
        config.mode = GPIO_MODE_INPUT_OUTPUT;
        break;

    default:
        config.mode = GPIO_MODE_OUTPUT;
        break;
    }

    switch (pullMode)
    {
    case PullMode_t::PullDown:
        config.pull_down_en = gpio_pulldown_t::GPIO_PULLDOWN_ENABLE;
        break;
    case PullMode_t::PullUp:
        config.pull_up_en = gpio_pullup_t::GPIO_PULLUP_ENABLE;
        break;
    default:

        break;
    }
    esp_err_t err = gpio_config(&config);
    return ConvertEspErrorToStatusCode(err);
}

uint8_t Gpio::Gpio_tToArduinoPin(Gpio_t gpio)
{
    return (uint8_t)Gpio_tTogpio_num_t(gpio);
}
#endif