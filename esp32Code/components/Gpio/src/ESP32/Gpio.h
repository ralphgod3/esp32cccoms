#if defined(__ESP32_ESP32__) || defined(ESP32)

#ifndef __GPIO_H__
#define __GPIO_H__

#include "../IGpio.h"
#include "driver/gpio.h"


/**
 * @brief list of pins for the esp32
 * @remark not all pins in this list might be on your microcontroller
 * 
 */
enum class Gpio_t
{
    NUM_NC = -1,    /*!< Use to signal not connected to S/W */
    NUM_0 = 0,     /*!< GPIO0, input and output */
    NUM_1 = 1,     /*!< GPIO1, input and output */
    NUM_2 = 2,     /*!< GPIO2, input and output */
    NUM_3 = 3,     /*!< GPIO3, input and output */
    NUM_4 = 4,     /*!< GPIO4, input and output */
    NUM_5 = 5,     /*!< GPIO5, input and output */
    NUM_6 = 6,     /*!< GPIO6, input and output */
    NUM_7 = 7,     /*!< GPIO7, input and output */
    NUM_8 = 8,     /*!< GPIO8, input and output */
    NUM_9 = 9,     /*!< GPIO9, input and output */
    NUM_10 = 10,   /*!< GPIO10, input and output */
    NUM_11 = 11,   /*!< GPIO11, input and output */
    NUM_12 = 12,   /*!< GPIO12, input and output */
    NUM_13 = 13,   /*!< GPIO13, input and output */
    NUM_14 = 14,   /*!< GPIO14, input and output */
    NUM_15 = 15,   /*!< GPIO15, input and output */
    NUM_16 = 16,   /*!< GPIO16, input and output */
    NUM_17 = 17,   /*!< GPIO17, input and output */
    NUM_18 = 18,   /*!< GPIO18, input and output */
    NUM_19 = 19,   /*!< GPIO19, input and output */
    NUM_20 = 20,   /*!< GPIO20, input and output */
    NUM_21 = 21,   /*!< GPIO21, input and output */
#if CONFIG_IDF_TARGET_ESP32
    NUM_22 = 22,   /*!< GPIO22, input and output */
    NUM_23 = 23,   /*!< GPIO23, input and output */

    NUM_25 = 25,   /*!< GPIO25, input and output */
#endif
    /* Note: The missing IO is because it is used inside the chip. */
    NUM_26 = 26,   /*!< GPIO26, input and output */
    NUM_27 = 27,   /*!< GPIO27, input and output */
    NUM_28 = 28,   /*!< GPIO28, input and output */
    NUM_29 = 29,   /*!< GPIO29, input and output */
    NUM_30 = 30,   /*!< GPIO30, input and output */
    NUM_31 = 31,   /*!< GPIO31, input and output */
    NUM_32 = 32,   /*!< GPIO32, input and output */
    NUM_33 = 33,   /*!< GPIO33, input and output */
    NUM_34 = 34,   /*!< GPIO34, input mode only(ESP32) / input and output(ESP32-S2) */
    NUM_35 = 35,   /*!< GPIO35, input mode only(ESP32) / input and output(ESP32-S2) */
    NUM_36 = 36,   /*!< GPIO36, input mode only(ESP32) / input and output(ESP32-S2) */
    NUM_37 = 37,   /*!< GPIO37, input mode only(ESP32) / input and output(ESP32-S2) */
    NUM_38 = 38,   /*!< GPIO38, input mode only(ESP32) / input and output(ESP32-S2) */
    NUM_39 = 39,   /*!< GPIO39, input mode only(ESP32) / input and output(ESP32-S2) */
#if GPIO_PIN_COUNT > 40
    NUM_40 = 40,   /*!< GPIO40, input and output */
    NUM_41 = 41,   /*!< GPIO41, input and output */
    NUM_42 = 42,   /*!< GPIO42, input and output */
    NUM_43 = 43,   /*!< GPIO43, input and output */
    NUM_44 = 44,   /*!< GPIO44, input and output */
    NUM_45 = 45,   /*!< GPIO45, input and output */
    NUM_46 = 46,   /*!< GPIO46, input mode only */
#endif
};

/**
 * @brief controls Gpio pins
 * 
 */
class Gpio : public IGpio
{
private:

    /**
     * @brief convert GpioMode to esp define
     * 
     * @param mode GpioMode
     * @return gpio_mode_t value of esp define
     */
    gpio_mode_t ConvertEspModeToMode(GpioMode_t mode);

    //block copying of object
    Gpio(const Gpio&) = delete; // copy constructor
    Gpio(Gpio&&) = delete; // copy constructor
    Gpio& operator=(const Gpio&) = delete; //copy assign
    Gpio& operator=(Gpio&&) = delete; //move assign
protected:
    /**
     * @brief Construct a new Pin Gpio object
     * 
     */
    Gpio(){};

    /**
     * @brief Destroy the Pin Gpio object
     * 
     */
    ~Gpio(){};

public:
    /**
     * @brief Get the Instance of the Gpio
     * 
     * @return Gpio& instance
     */
    static Gpio& GetInstance();

    /**
     * @brief initializes a gpio pin
     * 
     * @param pin pin to initialize
     * @param pullMode pulldown pullup or none
     * @param mode mode to initialize pin in
     * @return StatusCode_t status code
     */
    StatusCode_t SetPinMode(Gpio_t pin, PullMode_t pullMode, GpioMode_t mode);

    /**
     * @brief write pin status
     * 
     * @param state true = high, false = low
     */
    void WritePin(Gpio_t pin, bool state);

    /**
     * @brief read pin status
     * 
     * @return true pin is high
     * @return false pin is low
     */
    bool readPin(Gpio_t pin);


    /**
     * @brief convert Gpio_t to gpio_num_t
     * 
     * @param pin pin to convert
     * @return gpio_num_t result
     */
    static gpio_num_t Gpio_tTogpio_num_t(Gpio_t pin);

    /**
     * @brief Set the Gpio Pin Configuration, static function
     * 
     * @param pin pin to set config for
     * @param pullMode pullmode for pin
     * @param mode mode for pin
     */
    static StatusCode_t SetGpioPinConfig(Gpio_t pin, PullMode_t pullMode, GpioMode_t mode);

    /**
     * @brief convert pin to arduino pin
     * 
     * @param gpio pin to convert
     * @return uint8_t arduino pin equivelant
     */
    uint8_t Gpio_tToArduinoPin(Gpio_t gpio);
};

#endif
#endif