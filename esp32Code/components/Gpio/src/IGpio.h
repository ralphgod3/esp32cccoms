#ifndef __I_GPIO_H__
#define __I_GPIO_H__
#include "../../statuscodesandexceptions/src/StatusCodes.h"

/**
 * @brief direction that Gpio should be pulled
 * 
 */
enum class PullMode_t
{
    None,
    PullUp,
    PullDown
};

/**
 * @brief describes all GPIO pins for this microcontroller, should be defined in the implementation of the IGpio interface
 * 
 */
enum class Gpio_t;

/**
 * @brief mode that the gpio pin will operate in
 * 
 */

enum class GpioMode_t
{
    Disable,
    Input,
    Output,
    OutputOD,
    InputOutputOD,
    InputOutput
};

/**
 * @brief interface for standardized gpio control
 * 
 */
class IGpio
{
public:
    /**
     * @brief Destroy the IGpio object
     * 
     */
    virtual ~IGpio() = default;

    /**
     * @brief Set the Pin Mode for a pin
     * 
     * @param pin pin to set mode for
     * @param pMode pullup, pulldown, none
     * @param mode mode to set pin in
     * @return StatusCode_t 
     */
    virtual StatusCode_t SetPinMode(Gpio_t pin, PullMode_t pMode, GpioMode_t mode) = 0;
    
    /**
     * @brief write pin status
     * @param pin pin to write
     * @param state true = high, false = low
     */
    virtual void WritePin(Gpio_t pin, bool state) = 0;

    /**
     * @brief read pin status
     * @param pin pin to read
     * @return true pin is high
     * @return false pin is low
     */
    virtual bool readPin(Gpio_t pin) = 0;

    #ifdef ARDUINO
    /**
     * @brief convert Gpio_t to arduino pin equivalant
     * 
     * @param gpio gpio to convert
     * @return uint8_t arduino pin
     */
    virtual uint8_t Gpio_tToArduinoPin(Gpio_t gpio) = 0;
    #endif
};




#endif