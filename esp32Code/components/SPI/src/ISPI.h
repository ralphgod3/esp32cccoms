#ifndef ISPI_H
#define ISPI_H

#include <stdint.h>
#include "../../statuscodesandexceptions/src/StatusCodes.h"

/**
 * @brief standard interface definition for spi interactions
 * 
 */
class ISPI
{
public:
    virtual ~ISPI() = default;

    /**
     * @brief transmit an amount of data in blocking mode
     * 
     * @param pData pointer to data buffer
     * @param Size amount of bytes to transmit
     * @return StatusCode_t status code
     */
    virtual StatusCode_t Transmit(uint8_t *pData, uint16_t Size) = 0;
    
    /**
     * @brief receive an amount of data in blocking mode
     * 
     * @param pData pointer to data buffer
     * @param Size amount of data to be received
     * @return StatusCode_t status code
     */
    virtual StatusCode_t Receive(uint8_t *pData, uint16_t Size) = 0;

    /**
     * @brief transmit and receive data in blocking mode
     * 
     * @param pTxData pointer to transmit buffer
     * @param pRxData pointer to receive buffer
     * @param Size amount of data to be send and received
     * @return StatusCode_t status code
     */
    virtual StatusCode_t TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size) = 0;
};

#endif