#ifndef __I_M_STREAM_H__
#define __I_M_STREAM_H__
#include "../../statuscodesandexceptions/src/StatusCodes.h"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

/**
 * @brief interface for reading and writing
 * 
 */
class ImStream
{
public:

/**************************************** functions user should or may implement ******************************************/
    /**
     * @brief write data to stream
     * 
     * @param Characters data to write
     * @param Size size of the data in bytes
     * @return StatusCode_t status code
     */
    virtual StatusCode_t Write(const uint8_t* Characters, size_t Size) = 0;

    /**
     * @brief read multiple bytes from stream
     * 
     * @param buffer buffer bytes wil be read into
     * @param Size size of the buffer
     * @return size_t amount of bytes read
     */
    virtual size_t Read(uint8_t* buffer, size_t Size) = 0;

    /**
     * @brief check if new data is available for reading
     * 
     * @return size_t 0 if not available number of bytes available otherwise
     */
    virtual size_t Available() = 0;

    /**
     * @brief attach a callback for when data is received
     * 
     * @param fnc callback function
     * @param optionalParams optional user defined parameter
     * @return StatusCode_t status code
     */
    virtual StatusCode_t AttachReceiveInterrupt(void(*fnc)(uint8_t* buffer, size_t Size, void* optionalParams), void* optionalParams) = 0;

    /**
     * @brief detach an attached callback function
     * 
     * @return StatusCode_t status code
     */
    virtual StatusCode_t DetachReceiveInterrupt() = 0;

    /**
     * @brief used for some streams to handle hardware stuff, should be called periodically
     * 
     * @return StatusCode_t status code
     */
    virtual StatusCode_t Handle() { return StatusCode_t::OK;}

/**************************************** functions that should not be touched ******************************************/

    /**
     * @brief Destroy the Im Stream object
     * 
     */
    virtual ~ImStream() = default;

    /**
     * @brief write data do stream
     * 
     * @param Characters data to write
     * @param Size length of data
     * @return StatusCode_t status code
     */
    StatusCode_t Write(uint8_t* Characters, size_t Size)
    {
        return Write(const_cast<const uint8_t*>(Characters), Size);
    }
    
    /**
     * @brief write data do stream
     * 
     * @param Characters data to write
     * @param Size length of data
     * @return StatusCode_t status code
     */
    StatusCode_t Write(char* Characters, size_t Size)
    {
        return Write((uint8_t*)Characters, Size);
    }

    /**
     * @brief write data to stream
     * 
     * @param Character character to write
     * @return StatusCode_t status code
     */
    StatusCode_t Write(char Character)
    {
        return Write(&Character, 1);
    }

    /**
     * @brief write data to stream
     * 
     * @param Character character to write
     * @return StatusCode_t status code
     */
    StatusCode_t Write(uint8_t Character)
    {
        return Write(&Character, 1);   
    }

    /**
     * @brief write data do stream
     * 
     * @param Characters data to write
     * @param Size length of data
     * @return StatusCode_t status code
     */
    StatusCode_t Write(const char* Characters, size_t Size)
    {
        return Write((const uint8_t*)(Characters), Size);
    }

    /**
     * @brief write a formated string (printf)
     * 
     * @param format format of string
     * @param ... arguments
     * @return StatusCode_t status code 
     */
    StatusCode_t Writef(char* format, ...)
    {
        va_list args;
        va_start(args, format);
        char buf[200];
        vsnprintf(buf, 100, format, args);
        StatusCode_t ret = Write(buf, strlen(buf));
        va_end(args);
        return ret;
    }

    /**
     * @brief write a formated string (printf)
     * 
     * @param format format of string
     * @param ... arguments
     * @return StatusCode_t status code 
     */
    StatusCode_t Writef(const char* format, ...)
    {
        StatusCode_t ret = StatusCode_t::OK;
        va_list args;

        va_start(args, format);
        int reti = vsnprintf((char*)NULL, 0, format, args);
        char* vacmd = new char[reti + 1];
        va_end(args);
        
        va_start(args, format);
        reti = vsnprintf(vacmd,reti + 1, format, args);
        if (reti != -1)
        {
            ret = Write(vacmd, strlen(vacmd));
        }
        delete[] vacmd;
        va_end(args);
        return ret;
    }

    /**
     * @brief read multiple bytes from stream
     * 
     * @param buffer buffer bytes wil be read into
     * @param Size size of the buffer
     * @return size_t amount of bytes read 
     */
    size_t Read(char* buffer, size_t Size)
    {
        return Read((uint8_t*)buffer, Size);
    }

    /**
     * @brief read byte from stream
     * 
     * @return uint8_t 
     */
    uint8_t Read()
    {
        uint8_t out = 0;
        Read(&out, 1);
        return out;
    }
};


#endif