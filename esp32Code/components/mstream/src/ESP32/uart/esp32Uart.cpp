#if defined(__ESP32_ESP32__) || defined(ESP32)

#include "esp32Uart.h"
#include "esp_err.h"


esp32Uart::~esp32Uart(){}

StatusCode_t esp32Uart::Init(uart_port_t portNum, gpio_num_t tx, gpio_num_t rx)
{
    if(portNum == UART_NUM_MAX)
    {
        return StatusCode_t::INVALID_ARG;
    }
    if (p_isInitialized)
    {
        return StatusCode_t::ALREADY_INITIALIZED;
    }
    UARTSemaphore = xSemaphoreCreateBinary();

   if( UARTSemaphore  == NULL )
   {
       return StatusCode_t::INVALID_STATE;
   }
   xSemaphoreGive(UARTSemaphore);
   xSemaphoreTake(UARTSemaphore, portMAX_DELAY);
    
    uart_config_t uart_config = uart_config_t();
    uart_config.baud_rate = 115200;
    uart_config.data_bits = UART_DATA_8_BITS;
    uart_config.parity = UART_PARITY_DISABLE;
    uart_config.stop_bits = UART_STOP_BITS_1;
    uart_config.flow_ctrl = UART_HW_FLOWCTRL_DISABLE;
    uart_config.rx_flow_ctrl_thresh = 122;
    // Configure UART parameters
    esp_err_t err = uart_param_config(portNum, &uart_config);

    if(err != ESP_OK)
    {
        xSemaphoreGive(UARTSemaphore);
        return ConvertEspErrorToStatusCode(err);
    }
    this->port = portNum;

    err = uart_set_pin(portNum, tx, rx, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    if(err != ESP_OK)
    {
        xSemaphoreGive(UARTSemaphore);
        return ConvertEspErrorToStatusCode(err);
    }

    // Setup UART buffered IO with event queue
    const int uart_buffer_size = (1024 * 2);
    QueueHandle_t uart_queue;
    // Install UART driver using an event queue here
    err = uart_driver_install(portNum, uart_buffer_size, uart_buffer_size, 10, &uart_queue, 0);
    if(err != ESP_OK)
    {
        xSemaphoreGive(UARTSemaphore);
        return ConvertEspErrorToStatusCode(err);
    }

    p_isInitialized = true;
    xSemaphoreGive(UARTSemaphore);
    return StatusCode_t::OK;
}

StatusCode_t esp32Uart::Write(const uint8_t *Characters, size_t Size)
{
    if (!p_isInitialized)
    {
        return StatusCode_t::NOT_INITIALIZED;
    }
    if (Characters == nullptr || Size == 0)
    {
        return StatusCode_t::INVALID_ARG;
    }
    xSemaphoreTake(UARTSemaphore, portMAX_DELAY);
    uart_write_bytes(port, (const char*)Characters, Size);
    xSemaphoreGive(UARTSemaphore);
    Handle();
    return StatusCode_t::OK;
}

size_t esp32Uart::Read(uint8_t *buffer, size_t Size)
{
    if (!p_isInitialized || !Available() || buffer == nullptr || Size == 0)
    {
        return 0;
    }
    Handle();

    uint32_t outSize = 0;
    if (receiveBuffer.size() > Size)
    {
        memcpy(buffer, receiveBuffer.getHead(), Size);
        outSize = Size;
        receiveBuffer.pop_front(Size);
    }
    else
    {
        memcpy(buffer, receiveBuffer.getHead(), receiveBuffer.size());
        outSize = receiveBuffer.size();
        receiveBuffer.clear();
    }
    return outSize;
}

size_t esp32Uart::Available()
{
    if (!p_isInitialized)
    {
        return 0;
    }
    Handle();
    return receiveBuffer.size();
}

StatusCode_t esp32Uart::AttachReceiveInterrupt(void (*fnc)(uint8_t *buffer, size_t Size, void *optionalParams), void *optionalParams)
{
    if (!p_isInitialized)
    {
        return StatusCode_t::NOT_INITIALIZED;
    }
    if (fnc == nullptr)
    {
        return StatusCode_t::INVALID_ARG;
    }

    receiveCallbackFnc = fnc;
    receiveCallbackParams = optionalParams;
    return StatusCode_t::OK;
}

StatusCode_t esp32Uart::DetachReceiveInterrupt()
{
    if (!p_isInitialized)
    {
        return StatusCode_t::NOT_INITIALIZED;
    }
    receiveCallbackFnc = nullptr;
    receiveCallbackParams = nullptr;
    return StatusCode_t::OK;
}

StatusCode_t esp32Uart::Handle()
{
    if (!p_isInitialized)
    {
        return StatusCode_t::NOT_INITIALIZED;
    }

    // Read data from UART.
    
    size_t length = 0;
    esp_err_t err = ESP_OK;
    xSemaphoreTake(UARTSemaphore, portMAX_DELAY);
    err = uart_get_buffered_data_len(port, &length);
    if(err != ESP_OK)
    {
        xSemaphoreGive(UARTSemaphore);
        return ConvertEspErrorToStatusCode(err);
    }
    
    if(length != 0)
    {
        uint8_t* data = new uint8_t[length];
        length = uart_read_bytes(port, data, length, 100);

        receiveBuffer.push_back(data, length);
        delete[] data;
    }
    xSemaphoreGive(UARTSemaphore);
    

    if (receiveBuffer.size() > 0 && receiveCallbackFnc != nullptr)
    {
        //some memory shuffling so that should a receive interrupt hit mid callback we dont accidentally delete the data
        receiveCallbackFnc(receiveBuffer.getHead(), receiveBuffer.size(), receiveCallbackParams);
    }
    return StatusCode_t::OK;
}

#endif