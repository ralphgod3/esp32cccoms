#if defined(__ESP32_ESP32__) || defined(ESP32) 

#ifndef __ESP32_UART_H__
#define __ESP32_UART_H__

#include "driver/uart.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#include "../../ImStream.h"
#include "../../../../dynamicarray/src/dynamicArray.h"
#include "../../../../statuscodesandexceptions/src/StatusCodes.h"


//todo: add ability to set and get serial parameters
/**
 * @brief wrapper around esp32 uart class, mainly for debugging currently
 * 
 */
class esp32Uart : public ImStream
{
private:
    DynamicArray<uint8_t> receiveBuffer = DynamicArray<uint8_t>();
    uart_port_t port = uart_port_t();
    bool p_isInitialized = false;
    SemaphoreHandle_t UARTSemaphore = NULL;

    void (*receiveCallbackFnc)(uint8_t *buffer, size_t Size, void *optionalParams) = nullptr;
    void *receiveCallbackParams = nullptr;
public:
    /**
     * @brief Construct a new esp32 uart object
     * 
     */
    esp32Uart() {};
    /**
     * @brief Destroy the esp32 uart object
     * 
     */
    ~esp32Uart();

    /**
     * @brief initialize the uart class, standard settings 115200, 8,n,1
     * 
     * @param portNum port to use
     * @param tx tx pin
     * @param rx rx pin
     * @return StatusCode_t status code 
     */
    StatusCode_t Init(uart_port_t portNum, gpio_num_t tx, gpio_num_t rx);

    /**
     * @brief write data to stream
     * 
     * @param Characters data to write
     * @param Size size of the data in bytes
     * @return StatusCode_t status code
     */
    StatusCode_t Write(const uint8_t* Characters, size_t Size);

    /**
     * @brief read multiple bytes from stream
     * 
     * @param buffer buffer bytes wil be read into
     * @param Size size of the buffer
     * @return size_t amount of bytes read
     */
    size_t Read(uint8_t* buffer, size_t Size);

    /**
     * @brief check if new data is available for reading
     * 
     * @return size_t 0 if not available number of bytes available otherwise
     */
    size_t Available();

    /**
     * @brief attach a callback for when data is received
     * 
     * @param fnc callback function
     * @param optionalParams optional user defined parameter
     * @return StatusCode_t status code
     */
    StatusCode_t AttachReceiveInterrupt(void(*fnc)(uint8_t* buffer, size_t Size, void* optionalParams), void* optionalParams);

    /**
     * @brief detach an attached callback function
     * 
     * @return StatusCode_t status code
     */
    StatusCode_t DetachReceiveInterrupt();

    /**
     * @brief handle receiving of data and interrupting code
     * 
     * @return StatusCode_t status code
     */
    StatusCode_t Handle();
};



#endif
#endif