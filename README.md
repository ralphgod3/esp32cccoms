Small video showing the code in action.  
For any complaints about my filming Id like to see you try to type onehanded whilst holding a phone and trying to keep both displays in shot.  
[![Video](https://img.youtube.com/vi/cMosFKD_Px4/0.jpg)](https://www.youtube.com/watch?v=cMosFKD_Px4)

# Goal  
show people that it is quite easy to communicate with real hardware from computercraft and make something happen.  

# Limitations  
This code is writen as shite and should not be used to showcase how to do something, just that it is possible.  


# License  
Whilst I do not care what you do with main.cpp id rather not have you republish any of the files that are located in components.  
The reason for this is that these are part of a way bigger library that I have written that allows you to write code that works on any of the microcontrollers I support. (I removed all other implementations from components tho :))  
  
You should not need the components anyway for just interfacing I just used them because I have them and it makes it easier and faster to write code.  

An exemption to the limitations above is made for anyone using this with the express purpose of interfacing with computercraft for hobby use.  
tldr: DONT USE ANY CODE OUTSIDE OF A PROJECT THAT HAS TO DO WITH COMPUTERCRAFT AND DONT MAKE OR TRY TO MAKE MONEY OF OF IT.  

# Instructions
## lua  
Ensure you turn of the private blocking for http requests in your computercraft config.  
copy remoteTerminal.lua to a computercraft computer.  

## esp32  
Slightly more complex since I dislike Arduino.  
Install esp-idf latest release version (4.3.1) (If you only need it compiled for this then use the vscode esp-idf plugin, otherwise Id recomend installing it externally and then pointing the vscode plugin to your install for debugging ease later)  

Change the wifi ssid and password in main.cpp (yes im too lazy to do provisioning deal with it)  
The display should be attached to pin 5 and 4 and uses a default address of 0x3c if yours is different change the defines in main.cpp  

Compile and flash the code to an esp32 attached via usb (basicly run idf.py all flash monitor) If it says Idf.py is not recognized or a variation on this then ensure you have properly set the path variables and ran export.bat before trying to compile.  

## use
check what ip the esp32 got on its display and change the address in the lua code to that ip.  
run the program and start typing (enter is newline).  
